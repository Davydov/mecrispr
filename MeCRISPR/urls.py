from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns(
    '',

    url(r'^$', 'crispr.views.cassette_search_form', name='home'),
    url(r'^search$', 'crispr.views.cassette_search_results', name='search'),
    url(r'^blast$', 'crispr.views.blast_search', name='blast'),
    url(r'^blast/(?P<blast_id>\w+)$', 'crispr.views.blast_result',
        name='blast_result'),
    url(r'^hmmscan$', 'crispr.views.hmmscan_search', name='hmmscan'),
    url(r'^hmmscan/(?P<hmmscan_id>\w+)$', 'crispr.views.hmmscan_result',
        name='hmmscan_result'),
    url(r'^cassette/(?P<cassette_accession>\w+).fasta$',
        'crispr.views.cassette_fasta', name='fasta'),
    url(r'^spacer_blast/(?P<spacer_id>\w+)$',
        'crispr.views.spacer_blast_hits', name='spacer_blast'),
    url(r'^contig/(?P<contig_accession>[-|.\w]+).svg$',
        'crispr.views.contig_image', name='contig_image'),
    url(r'^contig/(?P<contig_accession>[-|.\w]+)$',
        'crispr.views.show_contig', name='contig'),
    url(r'^contig/fasta/(?P<contig_accession>[-|.\w]+).fasta$',
        'crispr.views.contig_fasta', name='contig_fasta'),
    url(r'^methods/$', 'crispr.views.list_methods',
        name='methods'),
    url(r'^metagenomes/$', 'crispr.views.list_metagenomes',
        name='metagenomes'),
    url(r'^about/$', 'django.contrib.flatpages.views.flatpage',
        {'url': '/about/'}, name='about'),
    url(r'^help/$', 'django.contrib.flatpages.views.flatpage',
        {'url': '/help/'}, name='help'),

    # url(r'^MeCRISPR/', include('MeCRISPR.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
