#!/usr/bin/env python
import sys
import csv

from Bio import SeqIO

seqs = {}

for record in SeqIO.parse(sys.argv[2], 'fasta'):
    seqs[record.id] = record

reader = csv.DictReader(open(sys.argv[1]))
writer = None
counter = 0

for record in reader:
    accession = record['accession']
    contig_accession = record['contig_accession']
    start = int(record['start'])
    end = int(record['end'])
    seq = seqs[contig_accession].seq
    if writer is None:
        writer = csv.DictWriter(sys.stdout, record.keys())
	writer.writeheader()
    left_extra = len(record['left_flank']) - 500
    if left_extra > 0:
	counter += 1
	record['left_flank'] = record['left_flank'][left_extra:]
    elif len(record['left_flank']) < min(start - 1, 500):
        print >> sys.stderr, 'Warning: short left flank for %s' % accession
	counter += 1
        record['left_flank'] = seq[
            max(int(start - 500 - 1), 0):start - 1]
    right_extra = len(record['right_flank']) - 500
    if right_extra > 0:
	counter += 1
	record['right_flank'] = record['right_flank'][
            :len(record['right_flank']) - right_extra]
    elif len(record['right_flank']) < min(len(seq) - end, 500):
        print >> sys.stderr, 'Warning: short right flank for %s' % accession
	counter += 1
        record['right_flank'] = seq[
            end:min(end + 500, len(seq))]
    writer.writerow(record)

print >> sys.stderr, 'Fixed %d flanks' %counter
