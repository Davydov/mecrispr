$(document).ready(function (){
    $('#cassette-search').submit(function() {

	$(this).find('input').filter(function (){
	    return !this.value;
	}).attr("disabled", true);
	$(this).find('select').filter(function (){
	    return this.value=='A';
	}).attr("disabled", true);
	return true;

    });

    $( window ).unload(function() {
	$('#cassette-search input, #cassette-search select').attr("disabled", false);
    });

    $("[href='" + window.location.pathname + "']").parents('li').addClass('active');

    $('[data-example]').each(function() {
	var q = $(this)
	var span = $(this).parent().nextAll('span.help-block:first');
	span.append('<a href="#" class="dashed example">[example]</a>');
	span.children('a.example').click(function() {
	    q.val(q.data('example'));
	    return false;
	});
    });

    hmmscan = $('pre.hmmscan');
    var t = hmmscan.text();
    hmmscan.html(t.replace(/(cluster#(\d)+)/g, '<a href="/search?repeat_cluster=$2">$1</a>'));
});
