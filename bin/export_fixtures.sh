#!/bin/bash
./manage.py dumpdata crispr.method --indent 2  > data/methods.json
./manage.py dumpdata crispr.metagenome --indent 2  > data/metagenomes.json
./manage.py dumpdata flatpages --indent 2 > data/flatpages.json
./manage.py dumpdata crispr.method crispr.metagenome flatpages --indent 2 > crispr/fixtures/initial_data.json
