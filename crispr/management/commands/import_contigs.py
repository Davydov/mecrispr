import sys

from django.core.management.base import BaseCommand
from crispr.models import Contig
from django.db import transaction


class Command(BaseCommand):
    help = 'Import CRISPR cassettes from stdin.'

    @transaction.commit_on_success
    def handle(self, *args, **kwargs):
        seq = ''
        ac = ''
        counter = 0
        for line in sys.stdin:
            line = line.strip()
            if line.startswith('>'):
                if seq and ac:
                    contig = Contig.objects.filter(
                        accession=ac).update(sequence=seq)
                    counter += 1
                    if counter % 1000 == 0:
                        print >> sys.stderr, '%d contigs imported' % counter
                ac = line[1:]
                seq = ''
            else:
                seq += line.upper()
        if seq and ac:
            contig = Contig.objects.filter(
                accession=ac).update(sequence=seq)
            counter += 1

        print >> sys.stderr, 'Sucessfully imported %d contigs.' % counter
