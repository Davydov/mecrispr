#!/usr/bin/env python
import sys
import argparse
import csv
import logging
import subprocess as sp
import tempfile
import os
import os.path
import shutil
from collections import defaultdict

from Bio.Seq import Seq


field_names = [
    'qacc', 'qstart', 'qend', 'qseq',
    'saccver', 'sstart', 'send', 'sseq',
    'mismatch', 'gaps', 'evalue']


def revcomp(s):
    return str(Seq(s).reverse_complement())


# Count mismatch
def mm(s1, s2):
    counter = 0
    for c1, c2 in zip(s1, s2):
        if c1.lower() != c2.lower():
            counter += 1
    return counter


def blast(infile, blastout, args):
    cmd = ['blastn',
           '-db', args.db,
           '-out', blastout,
           '-evalue', str(args.evalue),
           '-word_size', str(args.word_size),
           '-num_threads', str(args.num_threads),
           '-dust', args.dust,
           '-outfmt', '6 ' + ' '.join(field_names)]

    logging.debug('Starting blastn: %s' % cmd)
    p = sp.Popen(cmd,
                 stdin=sp.PIPE,
                 stdout=sp.PIPE,
                 stderr=sp.PIPE,
                 env=env)

    reader = csv.DictReader(infile)
    counter = 0
    for record in reader:
        for i, spacer in enumerate(record['spacers'].split()):
            spacer_id = "%s_S%02d" % (record['accession'], i)
            p.stdin.write('>%s\n%s\n' % (spacer_id, spacer))
            counter += 1
            if args.limit > 0 and counter >= args.limit:
                break

        if args.limit > 0 and counter >= args.limit:
            break

    logging.debug('Quering %d sequences' % counter)

    p.stdin.close()
    out = p.stdout.read()
    p.stdout.close()
    err = p.stderr.read()
    p.stderr.close()
    ret = p.wait()
    return ret, out, err


def get_sseq(blastout, args):
    reader = csv.DictReader(open(blastout), fieldnames=field_names,
                            dialect=csv.excel_tab)

    seqids = set()
    seqs = {}

    for record in reader:
        seqids.add(record['saccver'])
    logging.debug('Retriving %d sequences' % len(seqids))
    if len(seqids) == 0:
        logging.debug('Will not run blastdbcmd')
        return {}

    cmd = ['blastdbcmd',
           '-db', args.db,
           '-dbtype', 'nucl',
           '-entry_batch', '-',
           '-outfmt', '%a\t%t\t%s']

    logging.debug('Starting blastdbcmd: %s' % cmd)
    p = sp.Popen(cmd,
                 stdin=sp.PIPE,
                 stdout=sp.PIPE,
                 stderr=sp.PIPE,
                 env=env)

    for ac in seqids:
        p.stdin.write("%s\n" % ac)

    p.stdin.close()

    for line in p.stdout:
        ac, title, seq = line.split('\t')
        if ac in seqids:
            seqs[ac] = {'desc': title, 'seq': seq}

    p.stdout.close()
    err = p.stderr.read()
    p.stderr.close()
    ret = p.wait()

    if ret != 0:
        logging.critical('Error running blastdbcmd: %s' % err.strip())
        return None

    if len(seqids) != len(seqs):
        logging.critical("Couldn't fetch all sequences")
        return None

    return seqs


def get_qseq(blastout, args):
    reader = csv.DictReader(open(blastout), fieldnames=field_names,
                            dialect=csv.excel_tab)

    seqids = defaultdict(set)
    seqs = {}

    for record in reader:
        sac = record['qacc']
        ac, sn = sac.rsplit('_', 1)
        n = int(sn[1:])
        seqids[ac].add(n)

    logging.debug('Retriving repeat sequences for %d cassettes' % len(seqids))
    if len(seqids) == 0:
        logging.debug('Leaving function')
        return {}

    reader = csv.DictReader(open(args.cassettes))

    counter = 0
    for record in reader:
        if not record['accession'] in seqids:
            continue
        for i, spacer in enumerate(record['spacers'].split()):
            if i in seqids[record['accession']]:
                spacer_id = "%s_S%02d" % (record['accession'], i)
                seqs[spacer_id] = spacer
                counter += 1

    logging.debug('Retrived %d spacer sequences' % counter)

    return seqs


def show_result(blastout, qseqs, sseqs, outfile, args):
    reader = csv.DictReader(open(blastout), fieldnames=field_names,
                            dialect=csv.excel_tab)

    out_fields = ['cassette_accession', 'spacer_position', 'hit_description',
                  'hit_accession', 'evalue', 'query_string', 'hit_string']
    writer = csv.DictWriter(outfile, out_fields)
    writer.writeheader()

    mmcount = 0
    counter = 0

    for record in reader:
        qstart, qend = int(record['qstart']), int(record['qend'])
        sstart, send = int(record['sstart']), int(record['send'])
        qacc = record['qacc']
        sacc = record['saccver']
        if qend < qstart:
            log.critical('Query start > query end')
            return 6
        if send < sstart:
            sstrand = -1
        else:
            sstrand = +1
        if qstart > 1:
            add_begin = qstart - 1
        else:
            add_begin = 0
        if qend < len(qseqs[qacc]):
            add_end = len(qseqs[qacc]) - qend
        else:
            add_end = 0
        assert add_begin >= 0 and add_end >= 0
        subject_sequence = sseqs[sacc]['seq']
        if sstrand == 1:
            add_begin_seq = subject_sequence[max(sstart - 1 - add_begin, 0):
                                             sstart - 1]
            add_end_seq = subject_sequence[send:
                                           min(send + add_end,
                                               len(subject_sequence))]
        else:
            add_begin_seq = subject_sequence[sstart:
                                             min(sstart + add_begin,
                                                 len(subject_sequence))]
            add_begin_seq = revcomp(add_begin_seq)
            add_end_seq = subject_sequence[max(send - 1 - add_end, 0):send - 1]
            add_end_seq = revcomp(add_end_seq)
        add_begin_seq = '-' * (add_begin - len(add_begin_seq)) + add_begin_seq
        add_end_seq = add_end_seq + '-' * (add_end - len(add_end_seq))
        assert len(add_begin_seq) == add_begin and len(add_end_seq) == add_end

        new_query = (qseqs[qacc][:qstart - 1] +
                     record['qseq'] + qseqs[qacc][qend:])
        new_subject = add_begin_seq + record['sseq'] + add_end_seq
        assert len(new_query) == len(new_subject)
        assert new_query.replace('-', '') == qseqs[qacc]
        if sstrand == 1:
            snogap = new_subject.replace('-', '')
        else:
            snogap = revcomp(new_subject.replace('-', ''))
        assert snogap.replace('-', '') in subject_sequence
        if mm(new_query, new_subject) > args.max_mismatch:
            mmcount += 1
            continue
        ac, sn = record['qacc'].rsplit('_', 1)
        n = int(sn[1:])
        out_record = {'cassette_accession': ac, 'spacer_position': n,
                      'hit_description': sseqs[sacc]['desc'],
                      'hit_accession': sacc, 'evalue': record['evalue'],
                      'query_string': new_query, 'hit_string': new_subject}
        writer.writerow(out_record)
        counter += 1

    logging.info('%d hits expoted' % counter)
    logging.debug('%d ignored because of mismatch' % mmcount)


def main(infile, outfile, blastout, args):
    if args.blast_result:
        logging.info("reading %s instead of running blast" % args.blast_result)
        ret, out, err = 0, '', ''
    else:
        ret, out, err = blast(infile, blastout, args)

    if ret != 0:
        logging.critical('Error running blast: %s\n%s' %
                         (out.strip(), err.strip()))
        return 3
    else:
        logging.debug('blast: %s\b%s' % (out.strip(), err.strip()))

    sseqs = get_sseq(blastout, args)

    if sseqs is None:
        return 4

    qseqs = get_qseq(blastout, args)

    if qseqs is None:
        return 5

    if not sseqs or not qseqs:
        logging.warning("Blast found nothing")
        return 6

    return show_result(blastout, qseqs, sseqs, outfile, args)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Perform phage blast queries')
    parser.add_argument('db',
                        help='blast database')
    parser.add_argument('cassettes',
                        help='file with cassettes in csv format '
                        '(the same format as for import_cassettes)')
    parser.add_argument('--evalue', '-e', default=0.01, type=float,
                        help='E-value threshold (defalut: 0.01)')
    parser.add_argument('--db-path', '-p',
                        help='blast database path')
    parser.add_argument('--word-size', '-w', default=7, type=int,
                        help='Word size for wordfinder algorithm (default: 7)')
    parser.add_argument('--dust', '-D', default='no',
                        help='Filter query sequence with DUST '
                        '(see blastn docs, default: no)')
    parser.add_argument('--num-threads', '-n', default=1, type=int,
                        help='number of threads for blast (defalut 1)')
    parser.add_argument('--max-mismatch', '-m', default=4, type=int,
                        help='Maximum number of mismatches (default: 4)')
    parser.add_argument('--limit', '-l', default=0, type=int,
                        help='if >1 queries only first L spacers (defalut: 0)')
    parser.add_argument('--output', '-o',
                        help='filename to write result '
                        '(by default output to stdout)')
    parser.add_argument('--temporary', '-t',
                        help='path for temporary files '
                        '(on unix /tmp by default)')
    parser.add_argument('--debug', '-d', action='store_true',
                        help='show debug messages')
    parser.add_argument('--keep', '-k', action='store_true',
                        help='do not delete temporary files')
    parser.add_argument('--blast-result', '-b',
                        help="use provided blast results file "
                       "(don't run blast)")

    args = parser.parse_args()
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    try:
        inf = open(args.cassettes)
    except IOError as e:
        logging.critical('Error opening cassettes file: %s' % e)
        sys.exit(1)

    if args.output:
        try:
            outf = open(args.output, 'w')
        except IOError as e:
            logging.critical('Error opening output file: %s' % e)
            sys.exit(2)
    else:
        outf = sys.stdout

    env = os.environ.copy()
    if args.db_path:
        env['BLASTDB'] = args.db_path
        logging.debug('BLASTDB: %s' % env['BLASTDB'])

    tmpdir = tempfile.mkdtemp(suffix='phage-blast', dir=args.temporary)
    logging.debug('tmpdir: %s' % tmpdir)

    if not args.blast_result:
        blastout = os.path.join(tmpdir, 'blast.tab')
    else:
        blastout = args.blast_result

    try:
        ret = main(inf, outf, blastout, args)
    except KeyboardInterrupt:
        logging.info('Ctrl-C: exiting')
        ret = 7
    except:
        logging.exception('Unknown error during execution')
        ret = 8

    if not args.keep:
        logging.debug('deleting tmpdir')
        shutil.rmtree(tmpdir)
    elif not args.blast_result:
        logging.info('blast result saved in %s' % blastout)

    sys.exit(ret)
