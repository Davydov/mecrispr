from django.contrib import admin
from models import Metagenome, Method, Cassette, \
    Repeat, Spacer, Blast, BlastHit, HmmScan


class RepeatInline(admin.StackedInline):
    readonly_fields = ['sequence', 'position']
    model = Repeat
    max_num = 0


class SpacerInline(admin.StackedInline):
    readonly_fields = ['sequence', 'position']
    model = Spacer
    max_num = 0


class CassetteAdmin(admin.ModelAdmin):
    readonly_fields = ['contig']
    inlines = [RepeatInline, SpacerInline]


class BlastHitInline(admin.StackedInline):
    model = BlastHit
    max_num = 0
    readonly_fields = BlastHit._meta.get_all_field_names()


class BlastAdmin(admin.ModelAdmin):
    inlines = [BlastHitInline]


admin.site.register(Metagenome)
admin.site.register(Method)
admin.site.register(Cassette, CassetteAdmin)
admin.site.register(Blast, BlastAdmin)
admin.site.register(HmmScan)
