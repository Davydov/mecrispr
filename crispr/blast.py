import os.path
import subprocess as sp
import logging
from cStringIO import StringIO

from celery.exceptions import SoftTimeLimitExceeded

from MeCRISPR.settings import BLAST_PATH, BLAST_DB, BLAST_MAX_OUTPUT_SIZE
import models

field_names = [
    'qseqid', 'qlen', 'qstart', 'qend', 'qseq',
    'sseqid', 'slen', 'sstart', 'send', 'sseq',
    'pident', 'nident', 'length', 'mismatch',
    'gapopen', 'evalue', 'bitscore', 'score']


def run(b, force=False):
    if b.status != b.QUEUE and not force:
        logging.warning('%s (%d) has status %s. Skipping' % \
                            (str(b), b.id, b.get_status_display()))
        # This query already is processed.
        return

    # Security concerns.
    if b.program in (b.BLASTN, b.TBLASTN):
        ex = os.path.join(BLAST_PATH,
                          os.path.basename(b.program))
    else:
        logging.warning('Failed %s, unknown program.' % str(b))
        b.status = b.FAILED
        b.result = "Unknown program. Probably database corruption."
        b.save()
        return

    cmd = [ex,
           '-db', b.database,
           '-evalue', str(b.evalue),
           '-outfmt', '6 ' + ' '.join(field_names)]

    if b.short:
        # This comes from the old perl database:
        # -W 7 -F F
        # -W N Use words of size N (length of best perfect match,
        # 11 for blastn).
        # -F str Filter options for DUST or  SEG. Default: T.

        cmd.extend([
            '-word_size', '7',
            '-dust', 'no'])

    logging.info('Starting %s.' % str(b))

    b.status = b.RUNNING
    b.save()

    try:
        p = sp.Popen(cmd,
              stdin=sp.PIPE,
              stdout=sp.PIPE,
              stderr=sp.PIPE,
              env={'BLASTDB': BLAST_DB})

        # We don't use p.communicate since we cannot predict output size.
        p.stdin.write(b.query.encode('utf-8'))
        p.stdin.close()
        stdoutdata = p.stdout.read(BLAST_MAX_OUTPUT_SIZE)
        # Is the output truncated?
        if p.stdout.read(1):
            logging.warning('BLAST output is truncated')
            b.truncated = True
        p.stdout.close()
        stderrdata = p.stderr.read(BLAST_MAX_OUTPUT_SIZE)
        # Is the output truncated?
        if p.stderr.read(1):
            logging.warning('BLAST output (stderr) is truncated')
            b.truncated = True
        p.stderr.close()
        retcode = p.wait()
    except SoftTimeLimitExceeded:
        logging.warning('BLAST timeout on %s. Killing BLAST.' % str(b))
        p.kill()
        raise

    if retcode != 0:
        logging.warning('Failed %s.' % str(b))
        b.status = b.FAILED
        b.result = stdoutdata + stderrdata
        b.save()
        return

    hits = []
    for line in StringIO(stdoutdata):
        # if the output is truncated, we can get a partial line
        if not line.endswith('\n'):
            break
        if line.startswith('#'):
            # This is for format 7 with comments.
            pass
        d = dict(zip(field_names, line.rstrip('\n').split('\t')))
        hits.append(
            models.BlastHit(blast=b,
                            query_id=d['qseqid'], query_length=int(d['qlen']),
                            query_start=int(d['qstart']), query_end=int(d['qend']),
                            query_sequence=d['qseq'],
                            subject_id=d['sseqid'], subject_length=int(d['slen']),
                            subject_start=int(d['sstart']), subject_end=int(d['send']),
                            subject_sequence=d['sseq'],
                            percent_identity=float(d['pident']),
                            identity_count=int(d['nident']),
                            alignment_length=int(d['length']),
                            mismatches=int(d['mismatch']),
                            gap_opens=int(d['gapopen']),
                            bit_score=float(d['bitscore']),
                            score=float(d['score']),
                            evalue=float(d['evalue']))
        )

    models.BlastHit.objects.bulk_create(hits)

    b.status = b.COMPLETE
    b.save()

    logging.info('Finished %s.' % str(b))
