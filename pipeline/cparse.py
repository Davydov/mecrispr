import sys

from Bio.Alphabet import generic_dna
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Align import MultipleSeqAlignment
from Bio.Align import AlignInfo


# This class is used to output lists in ;-separated format in csvwriter
class SemicolonList(list):
    def __str__(self):
        return ';'.join(map(str, self))


class CRISPR(dict):
    def __init__(self, filename, *args, **kwargs):
        dict.__init__(self, *args, **kwargs)
        self.filename = filename
        self['spacers'] = SemicolonList()
        self['spacer_starts'] = SemicolonList()
        self['spacer_lengths'] = SemicolonList()
        self['repeats'] = SemicolonList()
        self['repeat_starts'] = SemicolonList()
        self['repeat_lengths'] = SemicolonList()

    def stat(self):
        self['max_sp_len'] = max(map(len, self['spacers']))
        self['min_sp_len'] = min(map(len, self['spacers']))
        self['avg_sp_len'] = (float(sum(map(len, self['spacers']))) /
                              len(self['spacers']))
        self['num_repeats'] = len(self['spacers']) + 1
        self['len_dr'] = len(self['dr_consensus'])

    def remove_gaps(self):
        self['dr_consensus'] = self['dr_consensus'].replace('-', '')
        if self['repeats']:
            if len(self['repeats']) != len(self['repeat_lengths']):
                raise DataError('number of repeats and lengths differ',
                                self.filename)
        else:
            return
        reported = False
        for i in xrange(len(self['repeats'])):
            if '-' in self['repeats'][i]:
                orig_length = len(self['repeats'][i])
                self['repeats'][i] = self['repeats'][i].replace('-', '')
                new_length = len(self['repeats'][i])
                if self['repeat_lengths'][i] == new_length:
                    # This is good behaviour. Do nothing.
                    pass
                elif self['repeat_lengths'][i] == orig_length:
                    self['repeat_lengths'][i] = new_length
                    if not reported:
                        warn('repeat length is calculated with gap',
                             self.filename)
                        reported = True
                else:
                    raise DataError('reported repeat length '
                                    'doesn\'t match actual repeat length')

    def check(self):
        self.stat()
        if self['repeat_lengths'][-1] <= 1:
            raise DataError('last repeat too short')
        if self['repeats']:
            if len(self['repeats']) != len(self['repeat_lengths']):
                raise DataError('number of repeats and lengths differ')
            if sum(map(len, self['repeats'])) != sum(self['repeat_lengths']):
                raise DataError('total length of repeats differ')
        if len(self['repeat_lengths']) != len(self['repeat_starts']):
            raise DataError('number of repeat lengthgs and starts differ')
        if not (len(self['spacers']) == len(self['spacer_lengths']) ==
                len(self['spacer_starts'])):
            raise DataError('number of spacers, spacer lengths '
                            'and spacer starts differ')
        if (sum(self['repeat_lengths']) + sum(self['spacer_lengths']) !=
                self['end'] - self['begin'] + 1):
            raise DataError('end - start differs from total length')
        if sum(map(len, self['spacers'])) != sum(self['spacer_lengths']):
            raise DataError('total length of spacers differ')
        if self['repeat_lengths'][-1] > self['len_dr'] + 1:
            raise DataError('last repeat is too long')
        if len(self['repeat_lengths']) != self['num_repeats']:
            raise DataError('number of repeats not equal to '
                            'number of spacers + 1')
        if self['repeat_lengths'][-1] > self['len_dr']:
            warn('last repeat (%d nt) is longer than '
                 'consensus repeat (%d nt)' %
                 (self['repeat_lengths'][-1], self['len_dr']), self.filename)


def warn(message, filename):
    print >> sys.stderr, 'Warning processing file %s: %s' % (filename, message)


def consensus(seqs):
    l = []
    i = 0
    for seq in seqs:
        l.append(SeqRecord((Seq(seq, generic_dna)), id=str(i)))
        i += 1

    a = MultipleSeqAlignment(l)
    summary_align = AlignInfo.SummaryInfo(a)
    return str(summary_align.dumb_consensus(0.5))

def column_width(l):
    res = []
    column = False
    start = None
    for i, c in enumerate(l):
        if not column and c == '=':
            start = i
            column = True
        elif column  and c != '=':
            end = i
            column = False
            res.append((start, end))
        elif not c in ('=', ' ', '\n'):
            raise FormatError('incorrect =line')
    start, end = res.pop()
    res.append((start, 0))
    return res

def get_columns(l, cw):
    res = []
    for start, end in cw:
        if end != 0:
            v = l[start:end]
        else:
            v = l[start:]
        res.append(v.strip())

    return res

def parse_cf(f):
    d = CRISPR(f.name)
    for line in f:
        if line.startswith('# Id:'):
            d['id'] = line.strip().split(':')[1][1:]
        elif line.startswith('# Crispr_begin_position:'):
            t = line.split()
            d['begin'] = int(t[2])
            d['repeat_starts'].append(int(t[2]))
            d['end'] = int(t[4])
        elif line.startswith('# DR:'):
            l = line.split()
            d['dr_consensus'] = l[2]
            d['repeat_lengths'].append(int(l[4]))
        elif line.startswith(' ' * 10):
            l = line.split()
            d['spacers'].append(l[2])
            d['spacer_starts'].append(int(l[0]))
            d['spacer_lengths'].append(int(l[1]))
            d['repeat_starts'].append(int(l[0]) + int(l[1]))
            d['repeat_lengths'].append(d['repeat_lengths'][-1])
    d['repeat_lengths'][-1] = d['end'] - d['repeat_starts'][-1] + 1
    try:
        d.check()
    except DataError as e:
        d.set_filename(f.name)
        raise d
    yield d


def parse_crt(f):
    for line in f:
        if line.startswith('ORGANISM:'):
            organism = line.split()[1]
        elif line.startswith('CRISPR'):
            d = CRISPR(f.name, id=organism)
            l = line.split()
            d['begin'] = int(l[3])
            d['end'] = int(l[5])
        elif line[0].isdigit():
            l = line.split()
            d['repeats'].append(l[1])
            d['repeat_starts'].append(int(l[0]))
            d['repeat_lengths'].append(len(l[1]))
            try:
                d['spacers'].append(l[2])
            except IndexError:
                pass
            else:
                d['spacer_starts'].append(int(l[0]) + int(l[4].rstrip(',')))
                d['spacer_lengths'].append(len(l[2]))
                if len(l[1]) != int(l[4].rstrip(',')):
                    raise DataError('reported repeat length differ from '
                                    'actual repeat length', f.name)
                if len(l[2]) != int(l[5]):
                    raise DataError('reported spacer length differ from '
                                    'actual spacer length', f.name)

        elif line.startswith('Repeats:'):
            d['dr_consensus'] = consensus(d['repeats'])
            try:
                d.check()
            except DataError as e:
                d.set_filename(f.name)
                raise d

            yield d
        elif line.startswith('No CRISPR elements were found.'):
            return


def combine(orig, modif):
    if len(orig) != len(modif):
        raise DataError('length of repeat differs from '
                        'length of non-consensus sequence')
    r = ''
    for i in xrange(len(orig)):
        if modif[i] != '.':
            r += modif[i]
        else:
            r += orig[i]
    return r


def parse_piler(f):
    for line in f:
        if line.startswith('DETAIL REPORT'):
            break
    else:
        return

    cw = None
    dl = []
    data_mode = False
    consensus_mode = False
    for line in f:
        if line.startswith('>'):
            dl.append(CRISPR(f.name))
            dl[-1]['id'] = line.split()[0][1:]
        elif line.startswith('Array'):
            narr = int(line.split()[1])
            if narr - 1 != len(dl):
                raise FormatError('array number not in order', f.name)
        elif line.startswith('=' * 10):
            if data_mode:
                consensus_mode = True
            else:
                try:
                    cw = column_width(line)
                except FormatError as e:
                    e.set_filename(f.name)
                    raise e
            data_mode = not data_mode
        elif data_mode:
            l = get_columns(line, cw)
            dl[-1]['repeat_starts'].append(int(l[0]))
            dl[-1]['repeat_lengths'].append(int(l[1]))
            dl[-1]['repeats'].append(l[5])
            if l[3]:
                dl[-1]['spacers'].append(l[6])
                dl[-1]['spacer_starts'].append(int(l[0]) + int(l[1]))
                dl[-1]['spacer_lengths'].append(int(l[3]))
        elif consensus_mode:
            l = line.split()
            dl[-1]['dr_consensus'] = l[3]
            consensus_mode = False
        elif line.startswith('SUMMARY BY SIMILARITY'):
            break
    else:
        raise FormatError('error parsing file '
                          '(no SUMMARY BY SIMILARITY line)', f.name)

    cw = None
    counter = 0
    for line in f:
        if line.startswith('=' * 5):
            try:
                cw = column_width(line)
            except FormatError as e:
                e.set_filename(f.name)
                raise e
            data_mode = not data_mode
        elif data_mode and not (line.strip().replace('*', '').
                                replace(' ', '').replace(':', '')):
            continue
        elif data_mode and line.startswith('SUMMARY BY POSITION'):
            break
        elif data_mode:
            l = get_columns(line, cw)
            i = int(l[0])
            dl[i-1]['begin'] = int(l[2])
            dl[i-1]['end'] = int(l[2]) + int(l[3]) - 1
            strand = l[7]
            cons = l[8].replace('-', '')
            if strand == '-':
                cons = str(Seq(cons).reverse_complement())
            elif strand != '+':
                raise FormatError('unknown strand %s' % strand, f.name)
            if dl[i-1]['dr_consensus'].replace('-', '') != cons:
                raise DataError('consensus sequence doesn\'t match', f.name)
            try:
                dl[i-1]['repeats'] = SemicolonList(
                    map(lambda m: combine(dl[i-1]['dr_consensus'], m),
                        dl[i-1]['repeats']))
            except DataError as e:
                e.set_filename(f.name)
                raise e
            dl[i-1].remove_gaps()
            try:
                dl[i-1].check()
            except DataError as e:
                e.set_filename(f.name)
                raise DataError(e.args[0], f.name)
            counter += 1
            yield dl[i-1]
    if counter != len(dl):
        raise FormatError('not all cassettes mentioned in'
                          'SUMMARY BY SIMILARITY section', f.name)


class Error(Exception):
    def __init__(self, message, filename=None):
        Exception.__init__(self, message)
        self.filename = filename

    def set_filename(self, filename):
        self.filename = filename


class FormatError(Error):
    pass


class DataError(Error):
    pass
