from datetime import timedelta
import csv

from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.template import RequestContext
from django.views.decorators.http import require_POST
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.http import HttpResponse, Http404

from forms import CassetteSearchForm, BlastForm, HmmScanForm
from models import Contig, Cassette, Method, Repeat, Spacer, PhageHit, \
    Blast, Metagenome, HmmScan, RepeatCluster
from utils import rainbow
from MeCRISPR.settings import TRUNCATE_EXPORT, RESULTS_EXPIRATION


ITEMS_PER_PAGE = 20
SHOW_PAGES = 5


def cassette_search_form(request):
    cassette_search_form = CassetteSearchForm()
    blast_form = BlastForm(prefix="blast")
    hmmscan_form = HmmScanForm(prefix="hmm")
    return render_to_response('crispr/main.html', {
            'cassette_search_form': cassette_search_form,
            'blast_form': blast_form,
            'hmmscan_form': hmmscan_form,
            }, context_instance=RequestContext(request))


def cassette_search_results(request):
    mq = Method.objects.all()
    form = CassetteSearchForm(request.GET, mq=mq)

    if not form.is_valid():
        return render_to_response('crispr/cassette_search.html', {
                'cassette_search_form': form,
                }, context_instance=RequestContext(request))

    contig_filter = False
    cq = Contig.objects
    for method in mq:
        fn = 'M_' + method.accession
        if form.cleaned_data[fn] == 'F':
            cq = cq.filter(methods=method)
            contig_filter = True
        elif form.cleaned_data[fn] == 'N':
            cq = cq.filter(~Q(methods=method))
            contig_filter = True

    ma = form.cleaned_data['metagenome']
    if ma:
        cq = cq.filter(metagenome__in=ma)
        contig_filter = True

    q = Cassette.objects.filter(status=Cassette.ACTIVE)

    if contig_filter:
        q = q.filter(contig__in=cq).distinct()

    accession = form.cleaned_data['accession']
    if accession:
        av = accession.split('.', 1)
        q = q.filter(accession=av[0])
        try:
            q = q.filter(version=int(av[1]))
        except IndexError:
            pass
        except ValueError:
            # Force nothing found.
            q = q.filter(version=-1)

    contig_accession = form.cleaned_data['contig_accession']
    if contig_accession:
        q = q.filter(contig__accession=contig_accession)

    high_quality = form.cleaned_data['high_quality']
    if high_quality:
        q = q.filter(high_quality=True)

    repeat_cluster = form.cleaned_data['repeat_cluster']
    if repeat_cluster:
        q = q.filter(repeat_cluster__pk=repeat_cluster)

    phage_blast = form.cleaned_data['phage_blast']
    if phage_blast:
        q = q.filter(spacer__phagehit__isnull=False)

    methods = form.cleaned_data['method']
    if methods:
        q = q.filter(method__in=methods)

    q = q.select_related('repeat_cluster', 'contig__metagenome', 'method',
                        'repeat_crisprdb')
    q = q.defer('contig__sequence')
    q = q.prefetch_related('spacer_set', 'repeat_set',
                           'spacer_set__phagehit_set')
    q = q.distinct()

    output = request.GET.get('format')
    sep = request.GET.get('sep')

    if output == 'csv':
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="cassettes.csv"'

        writer = csv.writer(response)
        # Excel delimiter depends on locale settings. Specify excplicitly.
        if sep:
            response.write('sep=%s\n' % writer.dialect.delimiter)
        writer.writerow([
                'accession',
                'full_accession',
                'version',
                'method',
                'contig', 'metagenome', 'start', 'end', 'strand',
                'sequence', 'left_flank', 'right_flank',
                'repeats', 'spacers',
                'repeat_consensus',
                'high_quality',
                'cluster',
                ])

        if TRUNCATE_EXPORT:
            q = q[:100]

        for c in q:
            repeats = ' '.join(map(lambda r: r.sequence, c.repeat_set.all()))
            spacers = ' '.join(map(lambda s: s.sequence, c.spacer_set.all()))
            if c.repeat_cluster:
                cluster_id = c.repeat_cluster.id
            else:
                cluster_id = ''
            writer.writerow([
                    c.accession,
                    '%s.%d' % (c.accession, c.version),
                    c.version,
                    c.method.accession,
                    c.contig.accession, c.contig.metagenome.accession, c.start, c.end, c.strand,
                    c.sequence, c.left_flank, c.right_flank,
                    repeats, spacers,
                    c.repeat_consensus,
                    c.high_quality,
                    cluster_id,
                    ])

        return response

    if output == 'contig_fasta':
        cq = Contig.objects.filter(cassette__in=q).distinct()

        response = render_to_response('crispr/contigs.fasta', {
                'contigs': cq}, context_instance=RequestContext(request),
                                      mimetype='chemical/seq-na-fasta')
        response['Content-Disposition'] = 'attachment; filename="contigs.fasta"'
        return response

    if output == 'repeat_fasta':
        if TRUNCATE_EXPORT:
            q = q[:100]

        response = render_to_response('crispr/repeats.fasta', {
                'cassettes': q}, context_instance=RequestContext(request),
                                      mimetype='chemical/seq-na-fasta')
        response['Content-Disposition'] = 'attachment; filename="repeats.fasta"'
        return response

    if output == 'spacer_fasta':
        if TRUNCATE_EXPORT:
            q = q[:100]

        response = render_to_response('crispr/spacers.fasta', {
                'cassettes': q}, context_instance=RequestContext(request),
                                      mimetype='chemical/seq-na-fasta')
        response['Content-Disposition'] = 'attachment; filename="spacers.fasta"'
        return response

    paginator = Paginator(q, ITEMS_PER_PAGE)
    page = request.GET.get('page')

    try:
        cassettes = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        page = 1
        cassettes = paginator.page(page)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        page = paginator.num_pages
        cassettes = paginator.page(page)

    page = int(page)
    page_min = max(page - SHOW_PAGES, 1)
    page_max = min(page + SHOW_PAGES, paginator.num_pages)
    pages = range(page_min, page_max)

    try:
        if pages[0] > 1:
            if pages[0] != 2:
                pages.insert(0, 0)
            pages.insert(0, 1)
        if pages[-1] < paginator.num_pages:
            if pages[-1] != paginator.num_pages - 1:
                pages.append(0)
            pages.append(paginator.num_pages)
    except IndexError:
        pass

    return render_to_response('crispr/cassette_search_results.html', {
            'cassettes': cassettes, 'pages': pages,
            'current_page': page, 'url': request.get_full_path(),
            'cassette_search_form': form, 'request_path': request.get_full_path()},
                              context_instance=RequestContext(request))


def cassette_fasta(request, cassette_accession):
    cassette = get_object_or_404(Cassette, accession=cassette_accession,
                                 status=Cassette.ACTIVE)

    response = render_to_response('crispr/cassettes.fasta', {
            'cassettes': [cassette]}, context_instance=RequestContext(request),
                                  mimetype='chemical/seq-na-fasta')
    response['Content-Disposition'] = 'attachment; filename="%s.fasta"' % cassette.accession

    return response


def contig_image(request, contig_accession):
    contig = get_object_or_404(Contig, accession=contig_accession)
    cassettes = Cassette.objects.filter(
        contig=contig, status=Cassette.ACTIVE).select_related().prefetch_related(
        'spacer_set', 'repeat_set')

    blast_id = request.GET.get('blast_id', '')
    query_id = request.GET.get('query_id', '')

    if blast_id:
        blast = get_object_or_404(Blast, pk=blast_id,
                                  status=Blast.COMPLETE)
        hits = blast.blasthit_set.order_by('subject_start')
    else:
        hits = []

    cassettes_processed = []
    min_start = None
    max_end = None
    for cassette in cassettes:
        c = {'start': cassette.start,
             'end': cassette.end,
             'elements': [],
             'method': str(cassette.method.accession),
             'type': 'cassette',
             }
        rss = filter(None,
                     reduce(lambda x, y: x + y,
                            map(None,
                                cassette.repeat_set.all(),
                                cassette.spacer_set.all())))
        start = c['start']
        for rs in rss:
            end = start + rs.length() - 1
            e = {'start': start,
                 'mid': float(start + end) / 2,
                 'end': end,
                 'length': rs.length()}
            if isinstance(rs, Repeat):
                e['type'] = 'repeat'
            else:
                e['type'] = 'spacer'
                if min_start is None or min_start > cassette.start:
                    min_start = cassette.start
                if max_end is None or max_end < cassette.end:
                    max_end = cassette.end
            start += rs.length()
            c['elements'].append(e)

        cassettes_processed.append(c)

    palette = rainbow(20)
    palette_stroke = rainbow(20, 0.8)

    c = {'type': 'hit',
         'elements': []}

    for hit in hits:
        if hit.subject_id == contig.accession and \
                hit.query_id == query_id:
            if hit.subject_end < hit.subject_start:
                hit.subject_start, hit.subject_end = (hit.subject_end,
                                                      hit.subject_start)
            e = {
                'start': hit.subject_start - 1,
                'end': hit.subject_end - 1,
                'length': hit.subject_end - hit.subject_start + 1,
                'type': 'hit',
                }
            c['elements'].append(e)

    if c['elements']:
        cassettes_processed.append(c)

    return render_to_response('crispr/contig.svg', {
            'contig': contig,
            'cassettes': cassettes_processed,
            'palette': palette,
            'palette_stroke': palette_stroke,
            'min_start': min_start,
            'max_end': max_end,
            }, context_instance=RequestContext(request),
                              mimetype='image/svg+xml')


def contig_fasta(request, contig_accession):
    contig = get_object_or_404(Contig, accession=contig_accession)

    response = render_to_response('crispr/contigs.fasta', {
        'contigs': [contig]}, context_instance=RequestContext(request),
                              mimetype='chemical/seq-na-fasta')
    return response


def show_contig(request, contig_accession):
    contig = get_object_or_404(Contig, accession=contig_accession)
    cassettes = Cassette.objects.filter(
        contig=contig, status=Cassette.ACTIVE).select_related(
        ).prefetch_related('spacer_set', 'repeat_set')

    return render_to_response('crispr/show_contig.html', {
            'contig': contig,
            'cassettes': cassettes,
            }, context_instance=RequestContext(request))


def spacer_blast_hits(request, spacer_id):
    spacer = get_object_or_404(Spacer, pk=spacer_id)
    cassette = spacer.cassette
    if not cassette.status == Cassette.ACTIVE:
        raise Http404
    hits = PhageHit.objects.filter(spacer=spacer)

    return render_to_response('crispr/spacer_blast_hits.html', {
            'spacer': spacer, 'hits': hits,
            'cassette': cassette},
                              context_instance=RequestContext(request))


@require_POST
def blast_search(request):
    blast_form = BlastForm(request.POST, prefix="blast")
    if not blast_form.is_valid():
        return render_to_response('crispr/blast_search.html', {
                'blast_form': blast_form,
                }, context_instance=RequestContext(request))

    blast = blast_form.save()

    blast.run()

    return redirect(blast_result, blast.id)


def blast_result(request, blast_id):
    blast = get_object_or_404(Blast, pk=blast_id)
    if blast.status == blast.COMPLETE:
        hits = blast.blasthit_set.all()
    else:
        hits = None

    expire = blast.created + timedelta(days=RESULTS_EXPIRATION)

    return render_to_response('crispr/blast_result.html', {
            'blast': blast,
            'hits': hits,
            'expire': expire},
                              context_instance=RequestContext(request))


@require_POST
def hmmscan_search(request):
    hmmscan_form = HmmScanForm(request.POST, prefix="hmm")
    if not hmmscan_form.is_valid():
        return render_to_response('crispr/hmmscan_search.html', {
                'hmmscan_form': hmmscan_form,
                }, context_instance=RequestContext(request))

    hmmscan = hmmscan_form.save()

    hmmscan.run()

    return redirect(hmmscan_result, hmmscan.id)


def hmmscan_result(request, hmmscan_id):
    hmmscan = get_object_or_404(HmmScan, pk=hmmscan_id)

    expire = hmmscan.created + timedelta(days=RESULTS_EXPIRATION)

    return render_to_response('crispr/hmmscan_result.html', {
            'hmmscan': hmmscan,
            'expire': expire},
                              context_instance=RequestContext(request))


def list_methods(request):
    methods = Method.objects.all()

    return render_to_response('crispr/list_methods.html', {
            'methods': methods},
                              context_instance=RequestContext(request))


def list_metagenomes(request):
    metagenomes = Metagenome.objects.all()

    return render_to_response('crispr/list_metagenomes.html', {
            'metagenomes': metagenomes},
                              context_instance=RequestContext(request))
