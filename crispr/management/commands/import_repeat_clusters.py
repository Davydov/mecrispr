import sys

from django.core.management.base import BaseCommand
from django.db import transaction

from crispr.models import RepeatCluster, Cassette


class Command(BaseCommand):
    help = 'Import repeat consensus clusters from stdin (blastclust format).'

    @transaction.commit_on_success
    def handle(self, *args, **kwargs):
        counter = 0
        # Skip first line
        sys.stdin.next()
        for line in sys.stdin:
            cassettes = line.strip().split(' ')
            if len(cassettes) <= 1:
                break
            counter += 1
            repeat_cluster = RepeatCluster()
            repeat_cluster.save()
            Cassette.objects.filter(accession__in=cassettes).update(
                repeat_cluster=repeat_cluster)
        print >> sys.stderr, 'Import %d repeat clusters.' % counter
