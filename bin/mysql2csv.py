#!/usr/bin/env python
import MySQLdb as mdb
import sys
import getpass
import csv
import argparse


def export_cassettes(cur, cw, metagenome):
    cur.execute("SELECT * FROM crispr_cassettes "
                "LEFT OUTER JOIN taxonomy "
                "ON taxonomy.contig_id=crispr_cassettes.contig_id "
                "WHERE if_duplicate!='1'")

    in_fields = ['cassette_id', 'cassette_seq',
                 'spacers', 'repeats', 'repeat_consensus',
                 'cass_start', 'cass_end', 'contig_id', 'repeat_strand',
                 'contig_length', 'contig_has_ca_hits', 'method', 'left_flank',
                 'right_flank', 'superb', 'short_cassette_id', 'description']

    out_fields = ['accession', 'sequence',
                  'spacers', 'repeats', 'repeat_consensus',
                  'start', 'end', 'contig_accession', 'strand',
                  'contig_length', 'contig_cas_hit', 'method', 'left_flank',
                  'right_flank', 'high_quality', 'short_accession', 'taxonomy',
                  'metagenome']

    assert len(in_fields) + 1 == len(out_fields)

    counter = 0
    cw.writerow(out_fields)
    for i in range(cur.rowcount):
        row = cur.fetchone()
        if row['description'] == 'N/a' or row['description'] == 'cas':
            row['description'] = ''
        l = []
        for f in in_fields:
            l.append(row[f])
        l.append(metagenome)
        cw.writerow(l)
        counter += 1

    print >> sys.stderr, 'Exported %d cassettes.' % counter


def export_phagehits(cur, cw):
    cur.execute("SELECT * FROM spacers_ncbi_viruses_blasts WHERE phage='1'")

    in_fields = ['cassette_id', 'spacer_position',
                 'hit_description', 'hit_genbank_id', 'evalue',
                 'query_string', 'hit_string',]
    out_fields = ['cassette_accession', 'spacer_position',
                  'hit_description', 'hit_accession', 'evalue',
                  'query_string', 'hit_string']

    assert len(in_fields) == len(out_fields)

    counter = 0
    cw.writerow(out_fields)
    for i in range(cur.rowcount):
        row = cur.fetchone()
        row['spacer_position'] = int(row['spacer_id'].rsplit('_spa_', 1)[1])
        l = []
        for f in in_fields:
            l.append(row[f])
        cw.writerow(l)
        counter += 1

    print >> sys.stderr, 'Exported %d phage blast hits.' % counter


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-H', '--host', help='mysql host',)
    parser.add_argument('-u', '--username', help='username')
    parser.add_argument('-p', '--password', help='password')
    parser.add_argument('-P', '--port', help='TCP port number',
                        type=int)
    parser.add_argument('-a', '--askpassword',
                        help='input password interactively',
                        action='store_true')
    parser.add_argument('-d', '--database',
                        help='database name (required)',
                        required=True)
    parser.add_argument('-m', '--metagenome',
                        help='metagenome name (default: GOS)',
                        default='GOS')
    parser.add_argument('cassettes', help='cassettes output file')
    parser.add_argument('phagehits', help='phage hits output file')
    args = parser.parse_args()

    cargs = {}
    if args.host:
        cargs['host'] = args.host
    if args.username:
        cargs['user'] = args.username
    if args.password:
        cargs['passwd'] = args.password
    if args.port:
        cargs['port'] = args.port
    if args.askpassword:
        cargs['passwd'] = getpass.getpass()
    cargs['db'] = args.database

    con = mdb.connect(**cargs)
    cur = con.cursor(mdb.cursors.DictCursor)

    cw = csv.writer(open(args.cassettes, 'w'))
    export_cassettes(cur, cw, args.metagenome)

    cw = csv.writer(open(args.phagehits, 'w'))
    export_phagehits(cur, cw)


if __name__ == '__main__':
    main()
