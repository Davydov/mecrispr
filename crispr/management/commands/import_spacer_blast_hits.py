import sys
import csv

from django.core.management.base import BaseCommand, CommandError
from crispr.models import Cassette, Spacer, PhageHit
from django.db import transaction


class Command(BaseCommand):
    help = 'Import spacers BLAST hits from stdin.'

    @transaction.commit_on_success
    def handle(self, *args, **kwargs):
        reader = csv.DictReader(sys.stdin)
        counter = 0
        for row in reader:
            cassette = Cassette.objects.get(
                accession=row['cassette_accession'])
            spacer = Spacer.objects.get(cassette=cassette,
                                        position=int(row['spacer_position']))
            hit = PhageHit(spacer=spacer,
                           hit_description=row['hit_description'],
                           hit_accession=row['hit_accession'],
                           evalue=float(row['evalue']),
                           query=row['query_string'],
                           hit=row['hit_string'])
            hit.save()

            counter += 1

        print >> sys.stderr, \
            'Sucessfully imported %d spacer BLAST hits.' % counter
