#!/usr/bin/env python
import sys

from Bio import AlignIO


AlignIO.write(AlignIO.read(sys.stdin, 'fasta'), sys.stdout, 'stockholm')
