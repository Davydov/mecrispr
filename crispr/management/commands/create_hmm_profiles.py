import sys
import os.path
import subprocess as sp
import logging

logging.basicConfig(level = logging.INFO)

from django.core.management.base import BaseCommand

from MeCRISPR.settings import HMMER_PATH, HMMER_DB, MUSCLE_PATH, PROJECT_ROOT
from crispr.models import Cassette



class Command(BaseCommand):
    help = 'Create HMMER profiles of clusters.'

    def handle(self, *args, **kwargs):
        cassettes = Cassette.objects.filter(repeat_cluster__isnull=False).order_by('repeat_cluster')
        cassettes = cassettes.select_related('repeat_cluster')

        muscle_cmd = [os.path.join(MUSCLE_PATH, 'muscle')]
        f2s_cmd = [os.path.join(PROJECT_ROOT, 'bin', 'fasta2stockholm.py')]
        hmmbuild_ex = os.path.join(HMMER_PATH, 'hmmbuild')
        hmm_clusters_fn = os.path.join(HMMER_DB, 'clusters.hmm')
        hmmpress_cmd = [os.path.join(HMMER_PATH, 'hmmpress'), '-f', hmm_clusters_fn]

        clusters = []

        counter = 0
        cl = None
        muscle = None

        logging.info('Got %d cassettes. This can take several minutes.' % len(cassettes))

        for cassette in cassettes:
            # New cluster
            if cl != cassette.repeat_cluster.id:
                cl = cassette.repeat_cluster.id
                clusters.append(cl)
                counter += 1
                if not muscle is None:
                    muscle.stdin.close()
                    muscle.wait()
                    f2s.wait()
                    hmmbuild.wait()
                muscle = sp.Popen(muscle_cmd,
                                  stdin=sp.PIPE,
                                  stdout=sp.PIPE,
                                  stderr=sp.PIPE)
                f2s = sp.Popen(f2s_cmd,
                               stdin=muscle.stdout,
                               stdout=sp.PIPE)
                hmmbuild_cmd = [hmmbuild_ex,
                                '--informat', 'stockholm',
                                '--rna',
                                '-n', 'cluster#%d' % cl,
                                '-o', '/dev/null',
                                os.path.join(HMMER_DB, '%d.hmm' % cl),
                                '-']
                hmmbuild = sp.Popen(hmmbuild_cmd,
                                    stdin=f2s.stdout)
                muscle.stdout.close()
                f2s.stdout.close()
            muscle.stdin.write('>%s\n%s\n' % 
                               (cassette.accession, cassette.repeat_consensus))
        muscle.stdin.close()
        muscle.wait()
        f2s.wait()
        hmmbuild.wait()

        logging.info('Created %d HMM profiles.' % counter)

        cf = open(hmm_clusters_fn, 'w')

        for cl in clusters:
            hmm_fn = os.path.join(HMMER_DB, '%d.hmm' % cl)
            hmm_f = open(hmm_fn)
            cf.write(hmm_f.read())
            hmm_f.close()
            os.remove(hmm_fn)

        cf.close()

        sp.call(hmmpress_cmd)

        os.remove(hmm_clusters_fn)

        logging.info('Created HMM database.')
