#!/usr/bin/env python
import sys
import csv
import re
from collections import defaultdict

from Bio import SeqIO

def nosp(s):
    return s.replace('-', '').replace('n', '').replace('N', '')

ALLOWED_SYM = set("ATGCRYSWKMBDHVN-")
CONS_ALLOWED_SYM = ALLOWED_SYM.union(set('X'))

def badsym(s):
    return ''.join(set(s.upper()).difference(ALLOWED_SYM))

def cons_badsym(s):
    return ''.join(set(s.upper()).difference(CONS_ALLOWED_SYM))

csvfile = sys.argv[1]
fastafile = sys.argv[2]

seqs = {}
hq = defaultdict(list)
ids = set()
cas = {}

for record in SeqIO.parse(open(fastafile), 'fasta'):
    seqs[record.id] = str(record.seq)

reader = csv.DictReader(open(csvfile))
contigs = set()
for row in reader:
    if len(row) == 1:
        print 'Incorrect number of rows, probably tabs instead of spaces'
        sys.exit()
    method = row['method']
    accession=row['accession']
    if not re.match(r'MC_\d{7}$', accession):
        print 'Incorrect cassette accession id (%s)' % accession
    ids.add(int(accession.split('_')[1]))
    contig_accession = row['contig_accession']
    contigs.add(contig_accession)
    if not re.match(r'[A-Z]+\|[a-zA-Z0-9_.-]+$', contig_accession):
        print 'Incorrect contig accession id %s (%s)' % (contig_accession, accession)

    sequence=row['sequence']
    left_flank=row['left_flank']
    right_flank=row['right_flank']
    if badsym(left_flank):
        print 'Incorrect symbols in left_flank (%s, %s)' % (accession, badsym(left_flank))
    if badsym(right_flank):
        print 'Incorrect symbols in right_flank (%s, %s)' % (accession, badsym(right_flank))

    high_quality=True if row['high_quality'] == '1' else False

    repeat_consensus=row['repeat_consensus']
    if cons_badsym(repeat_consensus):
        print 'Incorrect symbols in repeat_consensus (%s, %s)' % (accession, cons_badsym(repeat_consensus))
    start=int(row['start'])
    end=int(row['end'])
    contig_length = int(row['contig_length'])
    contig_cas_hit = True if row['contig_cas_hit'] else False
    try:
        if cas[contig_accession] != contig_cas_hit:
            print 'Inconsistent cas hit specification for contig %s' % contig_accession
    except KeyError:
        cas[contig_accession] = contig_cas_hit

    if high_quality:
        for hq_ac, hstart, hend in hq[contig_accession]:
            if not (
                    (start < hstart and end < hstart) or
                    (start > hend and end > hend)
                    ):
                print 'High quality cassettes overlap for contig %s (%s, %s)' % (contig_accession, hq_ac, accession)
        hq[contig_accession].append((accession, start, end))

    if end < start:
        print 'End < start (%s)' % accession

    if len(left_flank) > 500:
        print 'Left flank too long; should be max 500 (%s)' % accession

    if len(right_flank) > 500:
        print 'Right flank too long; should be max 500 (%s)' % accession

    if len(left_flank) !=  min(start - 1, 500):
        print 'Incorrect left flank length (%s)' % accession

    if len(right_flank) != min(contig_length - end, 500):
        print 'Incorrect right flank length (%s)' % accession

    if not seqs[contig_accession][:start-1].endswith(left_flank):
        print 'Incorrect left flank (%s)' % accession

    if not seqs[contig_accession][end:].startswith(right_flank):
        print 'Incorrect right flank (%s)' % accession

    metagenome = row['metagenome']
    if contig_accession.split('|')[0] != metagenome:
        print 'Incorrect contig accession (%s)' % accession

    if not contig_length == len(seqs[contig_accession]):
        print 'Incorrect contig length (%s)' % accession

    taxonomy=row['taxonomy']

    if taxonomy and taxonomy.count(';') != taxonomy.count('; '):
        print 'Incorrect taxonomy specification (%s)' % accession

    repeats = row['repeats'].split()
    spacers = row['spacers'].split()
    if len(spacers) < len(repeats) - 1:
        print 'Incorrect number of spacers vs repeats (%s)' % accession
    elif len(spacers) > len(repeats):
        print 'Too many spacers (%s)' % accession
    else:
        casseq = ''
        for i in xrange(len(repeats)):
            casseq += repeats[i]
            try:
                casseq += spacers[i]
            except IndexError:
                pass
        if badsym(casseq):
            print 'Incorrect symbols in cassette sequence (%s, %s)' % (accession, badsym(casseq))
        if nosp(casseq) != nosp(sequence):
            print 'Incorrect cassette sequence (%s)' % accession
        if nosp(casseq) != nosp(seqs[contig_accession][start-1:end]):
            print 'Cassette sequence and contig mismatch (%s)' % accession

        if end - start > len(casseq) - 1:
            print 'Cassette is to long (%s)' % accession
if len(ids) != max(ids) - min(ids) + 1:
    print 'some accessions are missing from the range'
if len(seqs) != len(contigs):
    print 'extra contigs in fasta file'
