import math


def rainbow(n, l=1):
    palette = [rgb2hex(*hsv2rgb(float(i) * 360 / n, 1, l)) for i in xrange(n)]
    return palette


def rgb2hex(r, g, b):
    return '#%02x%02x%02x' % (r, g, b)


# This code comes from:
# http://code.activestate.com/recipes/576919-python-rgb-and-hsv-conversion/
def hsv2rgb(h, s, v):
    h = float(h)
    s = float(s)
    v = float(v)
    h60 = h / 60.0
    h60f = math.floor(h60)
    hi = int(h60f) % 6
    f = h60 - h60f
    p = v * (1 - s)
    q = v * (1 - f * s)
    t = v * (1 - (1 - f) * s)
    r, g, b = 0, 0, 0

    if hi == 0:
        r, g, b = v, t, p
    elif hi == 1:
        r, g, b = q, v, p
    elif hi == 2:
        r, g, b = p, v, t
    elif hi == 3:
        r, g, b = p, q, v
    elif hi == 4:
        r, g, b = t, p, v
    elif hi == 5:
        r, g, b = v, p, q

    r, g, b = int(r * 255), int(g * 255), int(b * 255)
    return r, g, b


def wrap(s, max_length=80, sep='\n'):
    l = []
    for i in xrange(0, len(s), max_length):
        l.append(s[i:i + max_length])
    return sep.join(l)
