import sys
import os.path
import subprocess as sp
import logging

logging.basicConfig(level = logging.INFO)

from django.core.management.base import BaseCommand, CommandError

from MeCRISPR.settings import BLAST_PATH, BLAST_DB
from crispr.models import Repeat, Spacer, Contig
from crispr.utils import wrap



class Command(BaseCommand):
    help = 'Create BLAST databases.'

    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)
        self.ex = os.path.join(BLAST_PATH,
                               os.path.basename('makeblastdb'))
        
    def handle(self, *args, **kwargs):
        self.call('repeats')
        self.write_sequences(Repeat.objects.all().select_related(), 'repeat')
        self.wait()

        self.call('spacers')
        self.write_sequences(Spacer.objects.all().select_related(), 'spacer')
        self.wait()

        self.call('contigs')
        self.write_sequences(Contig.objects.all())
        self.wait()

    def call(self, name):
        filename = os.path.join(BLAST_DB,
                                name)

        cmd = [self.ex,
               '-dbtype', 'nucl',
               '-out', filename,
               '-title', name]

        logging.info("Creating %s database." % name)

        self.p = sp.Popen(cmd,
                          stdin=sp.PIPE,
                          stdout=sp.PIPE,
                          stderr=sp.PIPE)

    def write_sequences(self, qs, kind=''):
        try:
            total_count = qs.count()
            counter = 0
            logging.info("Will use %d sequences." % total_count)
            for i in xrange(0, total_count, 10000):
                for o in qs[i:i + 10000]:
                    if kind:
                        ac = "%s/%s/%s" % (o.cassette.accession,
                                           kind,
                                           o.position)
                    else:
                        ac = o.accession
                    seq = wrap(o.sequence.replace('-', ''))
                    self.p.stdin.write('>%s\n%s\n' % (ac, seq))
                    counter += 1
                logging.info("%d sequences" % counter)
        except IOError:
            logging.error('Error communicating with makeblastdb')
        
    def wait(self):
        self.p.stdin.close()

        sys.stdout.write(self.p.stdout.read())
        self.p.stdout.close()

        sys.stderr.write(self.p.stderr.read())
        self.p.stderr.close()

        self.p.wait()
