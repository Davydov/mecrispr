from django import forms
from django.utils.safestring import mark_safe

from models import Method, Blast, HmmScan, Metagenome


ACCESSION_EXAMPLE = 'MC_0001852.1'
CONTIG_ACCESSION_EXAMPLE = 'GOS|JCVI_SCAF_1096627147282'


class CassetteSearchForm(forms.Form):
    METHOD_CHOICES = (
        ('A', 'Any'),
        ('F', 'Found'),
        ('N', 'Not found'),
    )

    accession = forms.CharField(max_length=250, required=False,
                                widget=forms.TextInput(
            attrs={'data-example': ACCESSION_EXAMPLE}),
                                help_text=mark_safe(
            'Cassette accession can be specified as <code>MC_NNNNNNN</code> or '
            '<code>MC_NNNNNNN.VERSION</code>'))

    high_quality = forms.BooleanField(label='Only high-quality',
                                      required=False)

    repeat_cluster = forms.IntegerField(required=False,
                                        widget=forms.TextInput(
            attrs={'data-example': '3'}),
                                        help_text='Specify cluster accession')

    contig_accession = forms.CharField(max_length=250, required=False,
                                       widget=forms.TextInput(
            attrs={'data-example': CONTIG_ACCESSION_EXAMPLE}),
                                       help_text='Specify contig accession')

    phage_blast = forms.BooleanField(label='Phage BLAST hits',
                                     required=False)

    metagenome = forms.ModelMultipleChoiceField(queryset=Metagenome.objects.all(),
                                                required=False,
                                                help_text='Use Ctrl+click to select multiple items')

    method = forms.ModelMultipleChoiceField(queryset=Method.objects.all(),
                                            required=False,
                                            help_text='Filter cassettes by prediction methods. '
                                                      'Use Ctrl+click to select multiple items')

    def __init__(self, *args, **kwargs):
        try:
            mq = kwargs['mq']
            del(kwargs['mq'])
        except KeyError:
            mq = Method.objects.all()

        super(CassetteSearchForm, self).__init__(*args, **kwargs)

        for method in mq:
            field_name = 'M_' + method.accession
            self.fields[field_name] = forms.ChoiceField(
                choices=self.METHOD_CHOICES,
                required=False,
                label=method.name,
                initial='A')
            self.fields[field_name].group = ('Filter contigs',
                'You can limit search to contigs having '
                'certain combination of prediction methods')

        for field_name in self.fields:
            try:
                self.fields[field_name].group
            except AttributeError:
                self.fields[field_name].group = ''

    def clean(self):
        cleaned_data = super(CassetteSearchForm, self).clean()

        empty = True

        for field in cleaned_data:
            if cleaned_data[field]:
                empty = False

        if empty == True:
            raise forms.ValidationError("Empty request")

        return cleaned_data

BLAST_QUERY_EXAMPLE = """>sample_spacer
AAAATGATACTTCTGACTTCGTTGTGCGTAA
>sample_repeat
ATCCCTTTTTAGTCAGGGCATTTCTTCGAAC
>sample_piece_of_contig
AGCCTATATTGAATTAAGGTGATCTTCGAGGGTTCTGCCATCACAACTATTCCCTTAACTCCTCTAGCCAACAGGT
"""

class BlastForm(forms.ModelForm):
    class Meta:
        model = Blast
        fields = ['query', 'program', 'evalue',
                  'short', 'database']
        widgets = {
            'query': forms.Textarea(
                attrs={'data-example': BLAST_QUERY_EXAMPLE}
                ),
            }

HMMSCAN_QUERY_EXAMPLE = """>sample_repeat1
GAACTGAGACCCGATCTAAAGGGGATTAAGAC
>sample_repeat2
GCATCGCCCGGCTAAACAGCCGGGCGCGGATTGAAAC
"""

class HmmScanForm(forms.ModelForm):
    class Meta:
        model = HmmScan
        fields = ['query']
        widgets = {
            'query': forms.Textarea(
                attrs={'data-example': HMMSCAN_QUERY_EXAMPLE}
                ),
            }
