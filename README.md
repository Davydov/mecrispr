## About 

Me(tagenomic)CRISPR is a web-server which provides access to CRISPR
cassettes from metagenomic sequences found by a combination of three
algorithms.

The software is written in [Python](http://python.org) using
[Django](https://www.djangoproject.com/) framework.

## Install

### Requirements

First you should install required libraries. You will need
[virtualenv](http://www.virtualenv.org/) and [MySQL](http://mysql.com)
development headers.

On Debian/Ubuntu type:

    apt-get install python-virtualenv libmysqlclient-dev python-dev

You also will need
[BLAST2](ftp://ftp.ncbi.nlm.nih.gov/blast/executables/release/LATEST/),
[BLAST+](ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/),
[HMMER](http://hmmer.janelia.org/) and
[MUSCLE](http://www.drive5.com/muscle/muscle.html).

	apt-get install blast2 ncbi-blast+ hmmer muscle

### Get the code

Download and extract the
[source code](https://bitbucket.org/Davydov/mecrispr/get/master.tar.gz):

    tar zxvf Davydov-mecrispr-xxx.tar.gz
	cd Davydov-mecrispr-xxx

To build local environment type:

    bin/buildenv

## Environment

The software will **only** work in the virtual environment.
Enable the environment using:

	source me-env/bin/activate

## Configure

You have to create ``MeCRISPR/settings_local.py``. You can start from
the template ``MeCRISPR/settings_local.py.template``. If you are
running a production server don't forget to disable debug mode.

Run ``./manage.py syncdb`` to create new database. You have to create
superuser during this operation.

## Loading bundled data

The software is bundled with data from GOS. To load data into the database
type:

	# Import GOS cassettes.
	bzcat data/150814/1GOS_cassettes_150814.csv.bz2 | ./manage.py import_cassettes
	# Similar for HMP, JPN and DG
	# Import contig sequences.
	bzcat data/150814/1GOS_contigs_150814.fasta.bz2 | ./manage.py import_contigs
	# Similar for HMP, JPN and DG
	# Import phage blast hits.
	bzcat data/150814/Phage_hits_sorted.csv.bz2 | ./manage.py import_spacer_blast_hits

	# Proceed to this step once all metagenomic data is imported.
	# Create BLAST databases.
	./manage.py create_blast_db
	# Create repeat clusters.
	./manage.py create_clusters
	# Create HMM-profiles for the clusters.
	./manage.py create_hmm_profiles

	# Download latest CRISPRdb repeats file from http://crispr.u-psud.fr/crispr/BLAST/DR/DRdatabase
	...
	# Get taxonomy entries for individual repeats.
	./pipeline/get_dr_tax.py --output drtax DRdatabase
	# Add links to the CRISPRdb.
	./manage.py import_crisprdb < drtax

## Running locally

You have to run both server and the
[Celery](http://www.celeryproject.org/) worker. Celery worker is used
to run
[BLAST](http://blast.ncbi.nlm.nih.gov/Blast.cgi?CMD=Web&PAGE_TYPE=BlastDocs&DOC_TYPE=Download)
tasks. If you will not start the worker BLAST search wll not work.

To run builtin web server type:

    ./manage.py runserver

To run worker type (in other terminal window with virtual environment
enabled):

    ./manage.py celery worker --concurrency 1 --beat --loglevel=info

Then open in browser ```http://localhost:8000```.

### Imporing cassettes

Cassettes file is simple csv file which is parsed by Python
[csv library](http://docs.python.org/2/library/csv.html). The first
line in file is the header which should look somewhat like this (the
order doesn't matter):

    accession,sequence,spacers,repeats,repeat_consensus,start,end,contig_accession,strand,contig_length,contig_cas_hit,method,left_flank,right_flank,high_quality,taxonomy,metagenome

* ``accession`` — cassette accession like
  ``JCVI_SCAF_1101667000936_PIL_cass_0000``
* ``sequence`` — cassette nucleotide sequence
* ``spacers`` — list of spacers separated by the whitespace character
  (newline, space, etc.)
* ``repeats`` — list of repeats separated by the whitespace character
* ``repeat_consensus`` — repeat consensus sequence
* ``start`` — cassette start in contig
* ``end`` — cassette end in contig
* ``contig_accession`` — contig accession
* ``strand`` — cassette strand (if available), could be ``+``, ``-``
  or ``0`` if not available
* ``contig_length`` — the length of contig
* ``contig_cas_hit`` — set 1 if contig has Cas gene BLAST hit
* ``method`` — cassette prediction method, currently ``CFI``, ``CRT``
  or ``PIL``
* ``left_flank`` — contig left flank sequence
* ``right_flank`` — contig right flank sequence
* ``high_quality`` — set 1 if cassette in high quality dataset (please
  refer to the paper)
* ``taxonomy`` — taxonomy spearated by ``"; "`` (semicolon followed by
  a space symbol), e.g. ``Domain; Phylum; Class; ...``; empty if
  taxonomy is not available
* ``metagenome`` — short name of metagenome, like GOS

To load the data into the database type:

    ./manage.py import_cassettes < your_cassettes_file.csv

### Importing phage BLAST hits

Phage BLAST hits file contains information on spacer BLAST hits which
are virus DNA sequences. The first line in file is the header which
should look somewhat like this (the order doesn't matter):

    cassette_accession,spacer_position,hit_description,hit_accession,evalue,query_string,hit_string

* ``cassette_accession`` — cassette accession
* ``spacer_position`` — spacer position from start, 0-based
* ``hit_description`` — hit sequence description, e.g. ``Bacteriophage
  Mx8, complete genome``
* ``hit_accession`` — hit sequence accession, e.g.
  ``gb|AF396866.1|AF396866`` or ``gb|DQ222855.1|``
* ``evalue`` — E-value
* ``query_string`` — search query string,
  e.g. ``CTTCTTGGCGGCGGGCTTCTTC``
* ``hit_string`` — hit string, e.g. ``CTTCTTGGCGGCGGGCTTCTTC``

To load the data into the database type:

    ./manage.py import_spacer_blast_hits < your_spacer_blast_hits_file.csv

### Create repeat consensus clusters

Once all cassettes are in the database you should create repeat
sequence clusters. First make sure that blastclust (``blast2`` package
in Ubuntu) is installed on your system and ``BLAST2_PATH`` is set if
needed. Then run:

    ./manage.py create_clusters

### HMM profiles
First install [hmmer](http://hmmer.janelia.org/). You can set path to
hmmer executalbes using ``HMMER_PATH`` if needed. To create hmmer
profiles from all the clusters run:

    ./manage.py create_hmm_profiles

### BLAST databases

Once you have loaded all the cassettes and contigs you can create BLAST
databases of repeats, spacers and contigs. If you have BLAST+ installed, to do
so please run:

    ./manage.py create_blast_db

Please be sure to configure BLAST database path in
``MeCRISPR/settings_local.py`` first.

## Deployment

In production the software should be behind the web server like
[nginx](http://nginx.org/), [Lighttpd](http://www.lighttpd.net/) or
[Apache httpd](http://httpd.apache.org/).

The web server should be configured to serve static files in the
``static/`` directory of the database. Please refer to the
[Django manual](https://docs.djangoproject.com/en/1.5/howto/deployment/)
on configuration of the web server.

Please note that you have to run WSGI/FastCGI together with
Celery worker. You have to configure automatic start on server
boot. For example you can use [runit](http://smarden.org/runit/).

You can enable celery worker start on boot using provided init script.
Copy ``misc/celeryd`` to``/etc/init.d/celeryd`` and ``misc/celeryd.cfg``
to ``/etc/default/celeryd``. You probably want to edit
``/etc/default/celeryd`` first. Then use ``update-rc.d`` to enable the
init script.

## Converting data from the old database

MeCRISPR is bundled with ``bin/mysql2csv.py`` which allows you to
convert data to apropriate csv file format. If you have questions on
creating correct csv files please consider ``bin/mysql2csv.py`` source
code.
