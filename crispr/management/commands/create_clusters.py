import sys
import subprocess as sp
import os
import os.path
import logging

logging.basicConfig(level=logging.INFO)

from django.db import transaction
from django.core.management.base import BaseCommand, CommandError

from MeCRISPR.settings import BLAST2_PATH
from crispr.models import Cassette, RepeatCluster, Method, Contig


class Command(BaseCommand):
    help = 'Create repeat consensus clusters unsing blastclust.'

    @transaction.commit_on_success
    def handle(self, *args, **kwargs):
        method = Method.objects.get(accession='CRT')
        counter = 0

        ex = os.path.join(BLAST2_PATH, 'blastclust')

        cmd = [ex,
               '-p', 'F',
               '-L', '0.5',
               '-S', '50',
               '-e', 'F',
               '-p', 'F',
               '-W', '15']

        try:
            p = sp.Popen(cmd,
                         stdin=sp.PIPE,
                         stdout=sp.PIPE,
                         stderr=sp.PIPE)
        except OSError:
            raise CommandError('%s not found' % cmd[0])

        for cassette in Cassette.objects.filter(method=method):
            p.stdin.write('>%s\n%s\n' %
                          (cassette.accession, cassette.repeat_consensus))
            counter += 1
        p.stdin.close()
        logging.info('Clustering %d repeat consensus sequences.' % counter)

        p.stdout.next()
        counter = 0
        for line in p.stdout:
            cassette_acs = line.strip().split(' ')

            if len(cassette_acs) <= 1:
                break
            # Number of high-quality cassettes among the cluster.
            nhq = Cassette.objects.filter(accession__in=cassette_acs,
                                          high_quality=True).count()
            # Number of contigs per cluster.
            nco = Cassette.objects.filter(
                accession__in=cassette_acs).values('contig').distinct(
                ).count()

            if nhq > 0 and nco > 1: # Clusters with >0 HQ-cassettes and >1 contigs
                counter += 1
                repeat_cluster = RepeatCluster()
                repeat_cluster.save()
                Cassette.objects.filter(accession__in=cassette_acs).update(
                    repeat_cluster=repeat_cluster)

        logging.info('Created %d repeat clusters.' % counter)
        sys.stdout.write(p.stderr.read())
        p.wait()
        # Remove error log from blustclust
        os.remove('error.log')
