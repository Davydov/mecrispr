import sys
import csv

from django.core.management.base import BaseCommand, CommandError
from crispr.models import Cassette, Method, Metagenome, Repeat, Spacer, Contig
from django.db import transaction


class Command(BaseCommand):
    help = 'Import CRISPR cassettes from stdin.'

    @transaction.commit_on_success
    def handle(self, *args, **kwargs):
        reader = csv.DictReader(sys.stdin)
        metagenomes = {}
        methods = {}
        counter = 0
        for row in reader:
            if not row['method'] in methods:
                method = Method.objects.get(accession=row['method'])
                methods[row['method']] = method

            if not row['metagenome'] in metagenomes:
                metagenome = Metagenome.objects.get(
                    accession=row['metagenome'])
                metagenomes[row['metagenome']] = metagenome

            contig, created = Contig.objects.get_or_create(
                accession=row['contig_accession'],
                length=row['contig_length'],
                cas_hit=True if row['contig_cas_hit'] == '1' else False,
                metagenome=metagenomes[row['metagenome']],
                taxonomy=row['taxonomy'])

            cassette = Cassette(
                accession=row['accession'],
                sequence=row['sequence'].upper(),
                left_flank=row['left_flank'].upper(),
                right_flank=row['right_flank'].upper(),
                high_quality=True if row['high_quality'] == '1' else False,
                repeat_consensus=row['repeat_consensus'].upper(),
                contig=contig,
                start=row['start'],
                end=row['end'],
                method=methods[row['method']])

            if row['strand'] == '+':
                cassette.strand = '+'
            elif row['strand'] == '-':
                cassette.strand = '-'

            if row.has_key('version'):
                cassette.version = int(row['version'])

            if row.has_key('status'):
                cassette.status = row['status']

            cassette.save()

            for i, repeat_seq in enumerate(row['repeats'].split()):
                repeat = Repeat(cassette=cassette,
                                position=i, sequence=repeat_seq.upper())
                repeat.save()

            for i, spacer_seq in enumerate(row['spacers'].split()):
                spacer = Spacer(cassette=cassette,
                                position=i, sequence=spacer_seq.upper())
                spacer.save()

            counter += 1
            if counter % 1000 == 0:
                print >> sys.stderr, '%d cassetes imported' % counter
        print >> sys.stderr, 'Sucessfully imported %d cassettes.' % counter
