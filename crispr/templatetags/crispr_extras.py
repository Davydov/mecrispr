from urlparse import urlparse, urlunparse

from django.http import QueryDict
from django import template
from django.template import TemplateSyntaxError
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe

register = template.Library()

from crispr import utils

# This code comes from: http://stackoverflow.com/questions/5755150
# (altering-one-query-parameter-in-a-url-django)
@register.simple_tag
def replace_query_param(url, attr, val):
    (scheme, netloc, path, params, query, fragment) = urlparse(url)
    query_dict = QueryDict(query).copy()
    query_dict[attr] = val
    query = query_dict.urlencode()
    return urlunparse((scheme, netloc, path, params, query, fragment))


@register.filter
def wrap(value, arg):
    return utils.wrap(value, arg)


@register.filter(needs_autoescape=True)
def wrap_wbr(value, arg, autoescape=None):
    if autoescape:
        value = conditional_escape(value)
    return mark_safe(utils.wrap(value, arg, '<wbr>'))


# This code comes from:
# http://garmoncheg.blogspot.ru/2012/03/django-template-tags-to-find-out-field.html
@register.simple_tag(takes_context=True)
def set_this_field_type(context, field):
    """
    Adds to context given field type variable
    variable named "this_field_type"
    """
    context["this_field_type"] = field.field.__class__.__name__
    return ''


#Django template custom math filters
#Ref : https://code.djangoproject.com/ticket/361
@register.filter
def mult(value, arg):
    "Multiplies the arg and the value"
    return int(value) * int(arg)


@register.filter
def sub(value, arg):
    "Subtracts the arg from the value"
    return int(value) - int(arg)


@register.filter
def div(value, arg):
    "Divides the value by the arg"
    return int(value) / int(arg)


# This code mostly comes from:
# http://djangosnippets.org/snippets/1996/
class FloatWidthRatioNode(template.Node):
    def __init__(self, val_expr, max_expr, max_width, add):
        self.val_expr = val_expr
        self.max_expr = max_expr
        self.max_width = max_width
        self.add = add

    def render(self, context):
        try:
            value = float(self.val_expr.resolve(context))
            maxvalue = float(self.max_expr.resolve(context))
            max_width = float(self.max_width.resolve(context))
            add = float(self.add.resolve(context))
        except VariableDoesNotExist:
            return ''
        except ValueError:
            raise TemplateSyntaxError(
                "floatwidthratio final argument must be an number")

        return str(value / maxvalue * max_width + add)


@register.tag
def floatwidthratio(parser, token):
    bits = token.contents.split()
    if len(bits) < 4 or len(bits) > 5:
        raise TemplateSyntaxError("floatwidthratio takes three or fourarguments")
    tag, this_value_expr, max_value_expr, max_width = bits[:4]
    if len(bits) == 5:
        add = bits[4]
    else:
        add = '0'

    return FloatWidthRatioNode(parser.compile_filter(this_value_expr),
                               parser.compile_filter(max_value_expr),
                               parser.compile_filter(max_width),
                               parser.compile_filter(add))
