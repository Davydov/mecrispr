#!/usr/bin/env python
import sys
import glob
import argparse
import csv
import os.path

import cparse


def cl(l):
    return ';'.join(map(str, l))


def parse_guess(f):
    try:
        e = f.name.rsplit('.', 1)[1].lower()
    except IndexError:
        print >> sys.stderr, 'Error guessing %s.' % f.name
        return None
    if e in ('cf', 'cfi', 'cfp'):
        return cparse.parse_cf(f)
    elif e == 'crt':
        return cparse.parse_crt(f)
    elif e in ('pil', 'piler'):
        return cparse.parse_piler(f)
    print >> sys.stderr, 'Error guessing %s.' % f.name
    return None


def process(fp, path, pattern, of):
    out = csv.DictWriter(of, fieldnames=(
        'id', 'begin', 'end', 'dr_consensus', 'num_repeats', 'len_dr',
        'min_sp_len', 'max_sp_len', 'avg_sp_len',
        'spacer_starts', 'spacer_lengths', 'spacers',
        'repeat_starts', 'repeat_lengths', 'repeats'))
    out.writeheader()
    counter = 0
    errors = 0
    for fn in glob.iglob(os.path.join(path, pattern)):
        try:
            f = open(fn)
        except IOError as e:
            print >> sys.stderr, 'Error opening %s: %s' % (fn, e)
            continue
        try:
            crl = fp(f)
            if crl is None:
                continue
            for cr in crl:
                out.writerow(cr)
                counter += 1
        except cparse.Error as e:
            print >> sys.stderr, (
                'Error processing file %s: %s' % (e.filename, e.args[0]))
            errors += 1
        f.close()
    of.close()
    return counter, errors


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Parse crispr prediction files and produce csv output')
    parser.add_argument('--path', '-p', default='.',
                        help='path to search for files (default current path)')
    parser.add_argument('--pattern', '-n', default='*',
                        help='filename pattern; for example *.crt (default *)')
    parser.add_argument('--suffix', '-s',
                        help='filename suffix; for example .crt '
                        '(this will override pattern)')
    parser.add_argument('--output', '-o',
                        help='filename to write result '
                        '(by default output to stdout)')
    parser.add_argument('format', choices=['cf', 'crt', 'pil', 'guess'],
                        help='file format to use; '
                        'guess will try to guess by file extension '
                        '(.cf, .cfi, .cfp for CRISPRs Finder, '
                        '.crt for CRISPR Recognition Tool, '
                        '.pil or .piler for PILER)')
    args = parser.parse_args()
    if args.output:
        try:
            f = open(args.output, 'w')
        except IOError as e:
            print >> sys.stderr, 'Error opening output file: %s' % e
            sys.exit(1)
    else:
        f = sys.stdout
    if args.suffix:
        args.pattern = '*' + args.suffix
    if args.format == 'cf':
        fp = cparse.parse_cf
    elif args.format == 'crt':
        fp = cparse.parse_crt
    elif args.format == 'pil':
        fp = cparse.parse_piler
    elif args.format == 'guess':
        fp = parse_guess
    cnt, err = process(fp, args.path, args.pattern, f)
    if cnt > 0:
        print >> sys.stderr, 'Found %d cassettes, %d errors' % (cnt, err)
    else:
        print >> sys.stderr, 'No valid files found (%d errros)' % err
