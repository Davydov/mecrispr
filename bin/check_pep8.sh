#!/bin/sh
find . -path ./me-env -prune -o -name "*.py" -print | xargs pep8
