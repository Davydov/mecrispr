from datetime import timedelta

from django.utils.timezone import now

from celery import task
from celery.exceptions import SoftTimeLimitExceeded

import models
from blast import run as blast_run
from hmmscan import run as hmmscan_run
import hmmscan
from MeCRISPR.settings import RESULTS_EXPIRATION


@task(ignore_result=True)
def blast(bid):
    b = models.Blast.objects.get(pk=bid)
    try:
        blast_run(b)
    except SoftTimeLimitExceeded:
        b.status = b.FAILED
        b.result = 'BLAST timeout exceeded'
        b.save()
        b.hit_set.all().delete()


@task(ignore_result=True)
def hmmscan(hid):
    h = models.HmmScan.objects.get(pk=hid)
    try:
        hmmscan_run(h)
    except SoftTimeLimitExceeded:
        h.status = h.FAILED
        h.result = 'hmmscan timeout exceeded'
        h.save()


@task(ignore_result=True)
def blast_all():
    # This function runs all queued blasts. It will not be used in
    # production.
    for b in models.Blast.objects.filter(status=models.Blast.QUEUE):
        blast.run(b)


@task(ignore_result=True)
def hmmscan_all():
    # This function runs all queued hmmscans. It will not be used in
    # production.
    for h in models.HmmScan.objects.filter(status=models.HmmScan.QUEUE):
        hmmscan.run(h)


@task(ignore_result=True)
def delete_old():
    old_date = now()-timedelta(days=RESULTS_EXPIRATION)
    models.Blast.objects.filter(
        created__lte=old_date).delete()
    models.HmmScan.objects.filter(
        created__lte=old_date).delete()
