#!/usr/bin/env python
import sys
import csv
from collections import defaultdict

# check if two cassettes overlap
def overlap(s1, e1, s2, e2):
    assert s1 < e1
    assert s2 < e2
    if e1 < s2 or s1 > e2:
        return False
    return True

# All cassettes in each contig
# key - contig_accession
# value list of tuples: (cassette_accession, cassette_start, cassette_end)
contigs = defaultdict(list)

# cassette accessions to delete
delete = set()

# dit of cassette records. key is cassette accession
cassettes = {}

for record in csv.DictReader(open(sys.argv[1])):
    # For every enter in csv file

    # Save record to contigs
    contigs[record['contig_accession']].append((record['accession'],
            int(record['start']), int(record['end'])))

    # If no cas_hit & repeats <= 3 & spacer <= 2 & not hq
    if (record['contig_cas_hit'] != '1' and
            len(record['repeats'].split()) <= 3 and
            len(record['spacers'].split()) <= 2 and
            record['high_quality'] != '1'):
        # Save entry to delete set
        delete.add(record['accession'])

        # Save cassette (we need its coordinates later)
        cassettes[record['accession']] = record

# Now check overlap
for ac in delete.copy():
    # for every accession for deleteion

    # Get cassette record
    record = cassettes[ac]
    # cac - cassette accession
    cac = record['contig_accession']

    # for every cassettes in the same contig
    for oac, start, end in contigs[cac]:
        # if this is the same cassette don't check
        if oac == ac:
            continue
        # otherwise if this cassete overlaps with another
        # remove it from delete set
        if overlap(start, end, int(record['start']), int(record['end'])):
            delete.remove(ac)
            break

# Output cassette acessions in sorted order
for ac in sorted(delete):
    print ac
