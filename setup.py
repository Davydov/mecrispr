from setuptools import setup, find_packages
setup(
    name = "MeCRISPR",
    version = "0.8",
    packages = find_packages(),
    scripts = ['manage.py', 'bin/cluster_repeats.sh', 'bin/gen_key.py'],
    author = "Iakov Davydov",
    author_email = "iakov.davydov@gmail.com",
    url = "https://bitbucket.org/Davydov/mecrispr",
    )
