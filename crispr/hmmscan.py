import logging
import os.path
import subprocess as sp

from MeCRISPR.settings import HMMER_PATH, HMMER_DB, HMMER_MAX_OUTPUT_SIZE


def run(h, force=False):
    if h.status != h.QUEUE and not force:
        logging.warning('%s (%d) has status %s. Skipping' % \
                            (str(h), h.id, h.get_status_display()))
        # This query already is processed.
        return

    ex = os.path.join(HMMER_PATH, 'hmmscan')
    db = os.path.join(HMMER_DB, 'clusters.hmm')

    cmd = [ex,
           '--F1', '.1',
           db, '-']

    logging.info('Starting %s.' % str(h))

    h.status = h.RUNNING
    h.save()

    # Convert raw sequence to fasta.
    if not h.query.strip().startswith('>'):
        q = '>query\n' + h.query
    else:
        q = h.query

    try:
        p = sp.Popen(cmd,
              stdin=sp.PIPE,
              stdout=sp.PIPE,
              stderr=sp.PIPE)

        # We don't use p.communicate since we cannot predict output size.
        p.stdin.write(q.encode('utf-8'))
        p.stdin.close()
        stdoutdata = p.stdout.read(HMMER_MAX_OUTPUT_SIZE)
        p.stdout.close()
        stderrdata = p.stderr.read(HMMER_MAX_OUTPUT_SIZE)
        p.stderr.close()
        retcode = p.wait()
    except SoftTimeLimitExceeded:
        logging.warning('hmmscan timeout on %s. Killing hmmscan.' % str(b))
        p.kill()
        raise

    h.result = stdoutdata + stderrdata

    if retcode != 0:
        logging.warning('Failed %s, retcode=%d.' % (str(h), retcode))
        h.status = h.FAILED
        h.save()
        return

    h.status = h.COMPLETE
    h.save()

    logging.info('Finished %s.' % str(h))
