from django.core.management.base import BaseCommand

from crispr.models import Cassette


class Command(BaseCommand):
    help = 'Export repeat consensus sequence for all cassettes.'

    def handle(self, *args, **kwargs):
        for cassette in Cassette.objects.all():
            print '>%s\n%s' % (cassette.accession, cassette.repeat_consensus)
