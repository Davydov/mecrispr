#!/usr/bin/env python
# This scripts retrives a common taxonomy for repeats from the CRISPRdb list.
# The list is available at http://crispr.u-psud.fr/crispr/BLAST/DR/DRdatabase
import time
import random
import urllib2
import argparse
import pprint
import sys
import csv
import logging
import os.path

from lxml import etree


efetch_url = 'http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?' \
    'db={db}&id={id}&rettype={rettype}&retmode={retmode}&email=%s'
max_q = 500
timeout = 2
last_query = 0

def split(f):
    def new_f(l):
        d = {}
        l = list(l)
        for i in xrange(0, len(l), max_q):
            d.update(f(l[i:i + max_q]))
        return d
    return new_f

def id2ac(acs):
    return set(ac.rsplit('_', 1)[0] for ac in acs) 

def efetch(**kwargs):
    global last_query
    while time.time() - last_query < timeout:
        logging.debug('sleeping for 0.5 seconds')
        time.sleep(0.5)
    u = efetch_url.format(**kwargs)
    logging.debug('Efetch %s' % u)
    f = urllib2.urlopen(u)
    last_query = time.time()
    return f

@split
def get_taxids(acs):
    f = efetch(id=','.join(acs), db='nuccore', rettype='docsum', retmode='xml')
    d = {}
    root = etree.parse(f)
    for ds in root.iter('DocSum'):
        taxid, ac = None, None
        for it in ds.iter('Item'):
            name = it.get('Name')
            if name == 'TaxId':
                assert taxid is None
                taxid = it.text
            elif name == 'Caption':
                assert ac is None
                ac = it.text
        assert (not ac is None) and (not taxid is None)
        d[ac] = taxid
    return d

@split
def get_full_tax(taxids):
    f = efetch(id=','.join(taxids), db='taxonomy', rettype='',
               retmode='xml')
    root = etree.parse(f)
    d = {}
    for taxon in root.findall('./Taxon'):
        taxid = taxon.find('TaxId').text
        le = taxon.find('LineageEx')
        l = []
        for taxon_in in le.iter('Taxon'):
            taxid_in = taxon_in.find('TaxId').text
            name_in = taxon_in.find('ScientificName').text
            rank_in = taxon_in.find('Rank').text
            l.append((taxid_in, name_in, rank_in))
        d[taxid] = l
        # Find all AkaTaxIds as well
        akati = taxon.find('AkaTaxIds')
        if akati is not None:
            for taxid_in in akati.iter('TaxId'):
                d[taxid_in.text] = l
    return d

def common(sp1, sp2):
    if sp1 is None:
        return sp2
    if sp2 is None:
        return sp1
    for i, tax1 in enumerate(sp1):
        for tax2 in sp2:
            if tax1[0] == tax2[0]:
                break
        else:
            return sp1[0:i] 
    return sp1

def test_common(d):
    sp1, sp2 = random.sample(d.values(), 2)
    for i in xrange(min(len(sp1), len(sp2))):
        print '%50s\t%s' % (sp1[i], sp2[i])

    print common(sp1, sp2)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Convert DR file from CrisprDB to csv format')
    parser.add_argument('dr',
                        help='DR database file (DRdatabase.txt)')
    parser.add_argument('--debug', '-d', action='store_true',
                        help='Enable debugging output')
    parser.add_argument('--email', '-e',
                        help='E-mail for NCBI requests')
    parser.add_argument('--output', '-o',
                        help='Output filename')
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)

    email_path = os.path.expanduser("~/.mecrispr_email")
    if not args.email and os.path.exists(email_path):
        try:
            args.email = open(email_path).read().strip()
            logging.debug('Read e-mail %s from %s.' %
                          (args.email, email_path))
        except IOError, e:
            logging.error('Couldn\'t read e-mail from %s.' % email_path)
    while not args.email:
        args.email = raw_input("Enter your e-mail for the NCBI requests: ")
        if args.email:
            try:
                ef = open(email_path, 'w')
                print >> ef, args.email
                ef.close()
                logging.debug('E-mail %s saved to %s.' % (args.email, email_path))
            except IOError, e:
                logging.error('Couldn\'t write e-mail to %s.' % email_path)

    efetch_url = efetch_url % args.email


    if args.output:
        try:
            of = open(args.output, 'w')
        except IOError, e:
            logging.error('Couldn\'t open %s for writing (%s).' % (args.output,
                                                                   e))
            sys.exit(1)
    else:
        of = sys.stdout


    d = {}
    acs = set()
    f = open(args.dr)
    for line in f:
        if line.startswith('>'):
            ids = line[1:].strip().split('|')
            acs |= id2ac(ids)
        else:
            dr = line.strip()
            d[dr] = ids

    ac2tx = get_taxids(acs)
    tx2f = get_full_tax(ac2tx.values())

    w = csv.writer(of)
    w.writerow(('dr', 'taxid', 'name'))
    for dr in d:
        c = reduce(common, [tx2f[ac2tx[ac]] for ac in id2ac(d[dr])])
        if not c is None:
            w.writerow((dr, c[-1][0], c[-1][1]))
        else:
            logging.warning('Error fetching %s' % dr)

    of.close()
