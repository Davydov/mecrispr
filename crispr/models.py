from django.db import models
from django.core.validators import MaxLengthValidator

import tasks


def identity_string(seq1, seq2):
    l = ['|' if seq1[i] == seq2[i] else ' '
         for i in xrange(len(seq1))]
    return ''.join(l)


class Metagenome(models.Model):
    name = models.CharField(max_length=250)
    accession = models.CharField(max_length=250, unique=True)
    url = models.URLField(blank=True)
    description = models.TextField(blank=True)
    reference = models.TextField(blank=True)

    def __unicode__(self):
        return self.name


class Method(models.Model):
    name = models.CharField(max_length=250)
    accession = models.CharField(max_length=250, unique=True)
    url = models.URLField(blank=True)
    description = models.TextField(blank=True)
    reference = models.TextField(blank=True)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['accession', 'name']


class Contig(models.Model):
    accession = models.CharField(max_length=250, unique=True)
    length = models.IntegerField()
    cas_hit = models.BooleanField()
    metagenome = models.ForeignKey(Metagenome)
    taxonomy = models.TextField(blank=True)
    sequence = models.TextField(blank=True)

    methods = models.ManyToManyField(Method, through='Cassette')

    def __unicode__(self):
        return '<Contig %s>' % self.accession

    def taxnl(self):
        if self.taxonomy:
            return self.taxonomy.replace('; ', '\n')


class RepeatCluster(models.Model):
    def __unicode__(self):
        return '<RepeatCluster %d>' % self.id


class RepeatCrisprDB(models.Model):
    repeat_sequence = models.CharField(max_length=100, unique=True)
    taxid = models.IntegerField()
    name = models.TextField()

    def __unicode__(self):
        return '<RepeatCrisprDB %s>' % self.name


class Cassette(models.Model):
    STRAND = (
        ('+', 'Forward'),
        ('-', 'Reverse'))

    ACTIVE = 'A'
    OUTDATED = 'O'
    DELETED = 'D'
    STATUS = (
        (ACTIVE, 'Active'),
        (OUTDATED, 'Outdated'),
        (DELETED, 'Deleted'))

    accession = models.CharField(max_length=16, db_index=True)
    version = models.IntegerField(default=1)
    status = models.CharField(max_length=1, choices=STATUS, default=ACTIVE,
                              db_index=True)
    sequence = models.TextField()
    left_flank = models.TextField()
    right_flank = models.TextField()
    high_quality = models.BooleanField()
    repeat_consensus = models.TextField()
    repeat_cluster = models.ForeignKey(RepeatCluster, blank=True, null=True)
    repeat_crisprdb = models.ForeignKey(RepeatCrisprDB, blank=True, null=True)
    contig = models.ForeignKey(Contig)
    start = models.IntegerField()
    end = models.IntegerField()
    strand = models.CharField(max_length=1, choices=STRAND, blank=True)
    method = models.ForeignKey(Method)

    def __unicode__(self):
        return "<Cassette %s>" % self.accession

    class Meta:
        ordering = ['contig', 'accession']
        unique_together = ('accession', 'version')


class Spacer(models.Model):
    cassette = models.ForeignKey(Cassette)
    position = models.IntegerField()
    sequence = models.TextField()

    def __unicode__(self):
        return "<Spacer %s/%s>" % (
            self.cassette.accession,
            self.position)

    def has_phagehits(self):
        return self.phagehit_set.count() > 0

    def length(self):
        return len(self.sequence.replace('-', ''))

    class Meta:
        unique_together = ('cassette', 'position')
        ordering = ['cassette', 'position']


class Repeat(models.Model):
    cassette = models.ForeignKey(Cassette)
    position = models.IntegerField()
    sequence = models.TextField()

    def length(self):
        return len(self.sequence.replace('-', ''))

    def __unicode__(self):
        return "<Repeat %s/%s>" % (self.cassette.accession,
                                   self.position)

    class Meta:
        unique_together = ('cassette', 'position')
        ordering = ['cassette', 'position']


class PhageHit(models.Model):
    spacer = models.ForeignKey(Spacer)
    hit_description = models.CharField(max_length=500)
    hit_accession = models.CharField(max_length=100)
    evalue = models.FloatField()
    query = models.CharField(max_length=100)
    hit = models.CharField(max_length=100)

    def identity(self):
        return identity_string(self.query, self.hit)

    class Meta:
        ordering = ['evalue']


class Deferred(models.Model):
    QUEUE = 'q'
    RUNNING = 'r'
    COMPLETE = 'c'
    FAILED = 'f'
    STATUS_CHOICES = (
        (QUEUE, 'in queue'),
        (RUNNING, 'running'),
        (COMPLETE, 'complete'),
        (FAILED, 'failed'),
    )

    status = models.CharField(choices=STATUS_CHOICES,
                              max_length=1,
                              default=QUEUE)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Blast(Deferred):
    BLASTN = 'blastn'
    TBLASTN = 'tblastn'
    PROGRAM_CHOICES = (
        (BLASTN, 'blastn, nucleotide query'),
        (TBLASTN, 'tblastn, protein query'),
    )

    SPACERS = 'spacers'
    REPEATS = 'repeats'
    CONTIGS = 'contigs'
    DB_CHOICES = (
        (SPACERS, 'spacers'),
        (REPEATS, 'repeats'),
        (CONTIGS, 'contigs'),
    )

    query = models.TextField(validators=[MaxLengthValidator(8192)],
                             help_text='Raw or FASTA sequence')
    result = models.TextField(blank=True)
    truncated = models.BooleanField(default=False)
    program = models.CharField(
        choices=PROGRAM_CHOICES,
        max_length=16,
        default=BLASTN)
    evalue = models.FloatField('E-value', default=0.01)
    short = models.BooleanField(
        'correction for short queries (<30nt)')
    database = models.CharField(
        choices=DB_CHOICES,
        max_length=16,
        default=CONTIGS)


    def run(self):
        tasks.blast.delay(self.id)

    def __unicode__(self):
        return "<Blast %s %s>" % (self.created.strftime('%d-%m-%y %H:%M'),
                                  self.status.upper())


class BlastHit(models.Model):
    blast = models.ForeignKey(Blast)
    query_id = models.CharField(max_length=250)
    query_length = models.IntegerField()
    query_start = models.IntegerField()
    query_end = models.IntegerField()
    query_sequence = models.TextField()
    subject_id = models.CharField(max_length=250)
    subject_length = models.IntegerField()
    subject_start = models.IntegerField()
    subject_end = models.IntegerField()
    subject_sequence = models.TextField()
    percent_identity = models.FloatField()
    identity_count = models.IntegerField()
    alignment_length = models.IntegerField()
    mismatches = models.IntegerField()
    gap_opens = models.IntegerField()
    bit_score = models.FloatField()
    score = models.IntegerField()
    evalue = models.FloatField()

    def identity(self):
        return identity_string(self.query_sequence,
                               self.subject_sequence)

    def subseq(self, max_length=80):
        idn = self.identity()
        query_strand = 1 if self.query_start <= self.query_end else -1
        subject_strand = 1 if self.subject_start <= self.subject_end else -1
        for i in xrange(0, len(self.query_sequence), max_length):
            j = min(i + max_length, len(self.query_sequence))
            q_s = self.query_start + query_strand * i
            q_e = self.query_start + query_strand * (j - 1)
            s_s = self.subject_start + subject_strand * i
            s_e = self.subject_start + subject_strand * (j - 1)

            d = {'query_sequence': self.query_sequence[i:j],
                 'query_start': q_s,
                 'query_end': q_e,
                 'subject_sequence': self.subject_sequence[i:j],
                 'subject_start': s_s,
                 'subject_end': s_e,
                 'identity': idn[i:j],
                 }
            yield d

    class Meta:
        ordering = ['pk']


class HmmScan(Deferred):
    query = models.TextField(validators=[MaxLengthValidator(8192)],
                             help_text='Raw or FASTA sequence')
    result = models.TextField(blank=True)

    def run(self):
        tasks.hmmscan.delay(self.id)

    def __unicode__(self):
        return "<HmmScan %s %s>" % (self.created.strftime('%d-%m-%y %H:%M'),
                                    self.status.upper())
