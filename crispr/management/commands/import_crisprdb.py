import sys
import csv

from django.core.management.base import BaseCommand
from django.db import transaction

from Bio import Seq

from crispr.models import Cassette, RepeatCrisprDB


def revcomp(s):
    return str(Seq.Seq(s).reverse_complement())

class Command(BaseCommand):
    help = 'Import CrisprDB data in csv format from stdin.'

    @transaction.commit_on_success
    def handle(self, *args, **kwargs):
        counter = 0
        r = csv.DictReader(sys.stdin)
        dr = {}
        for record in r:
            dr[record['dr']] = record
            # Reverse complement of dr
            dr[revcomp(record['dr'])] = record

        dr_d = {}
        for cassette in Cassette.objects.all():
            rs = cassette.repeat_consensus
            if rs in dr:
                counter += 1
                try:
                    cassette.repeat_crisprdb = dr_d[rs]
                except KeyError:
                    rdc = RepeatCrisprDB(
                        repeat_sequence=rs,
                        taxid=dr[rs]['taxid'],
                        name=dr[rs]['name'])
                    rdc.save()
                    dr_d[rs] = rdc
                    dr_d[revcomp(rs)] = rdc
                    cassette.repeat_crisprdb = rdc
                cassette.save()

        print >> sys.stderr, 'Added CrisprDB reference to %d cassettes.' % counter
