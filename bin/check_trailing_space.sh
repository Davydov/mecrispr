#!/bin/sh
find . -path ./me-env -prune -o \( -name "*.py"  -o -name "*.html" -o -name "*.js" -o -name "*.css" -o -name "*.sh" \) -print \
| xargs grep --files-with-matches '[[:blank:]]$'
